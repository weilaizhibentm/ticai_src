@echo off

call env.bat

if /i "%1" == "newkey" (
  cd MSM8937.LA.3.0
  cd common\sectools\resources\data_prov_assets\Signing\Local\oem_certs\
  call makeCrt.bat
  cd ..\..\..\..\..\..\..\
  cd ..
) else if /i "%1" == "sec.dat" (
  cd MSM8937.LA.3.0
  python common/sectools/sectools.py fuseblower -p 8937 -g -v
  cp common\sectools\fuseblower_output\v2\sec.dat common\sectools\resources\build\fileversion2\sec.dat
  cd ..
) else if /i "%1" == "" (
  cd MSM8937.LA.3.0
  python common/sectools/sectools.py secimage -m . -p 8937 -sa
  python common/sectools/copy_sec_img.py
  python common/sectools/sectools.py  secimage  -i common/sectools/prog_emmc_firehose_8937_ddr_quectel.mbn  -c common/sectools/config/8937/8937_secimage.xml -g prog_emmc_firehose_ddr -sa
  cd common\build
  python build.py
  cd ../../../
) else (
  echo invalid param!
)
