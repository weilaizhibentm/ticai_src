#########################################################################
# File Name: auto_out.sh
# Author: 
# mail: 
# Created Time: 2016年11月22日 星期二 16时41分09秒
#########################################################################
#!/bin/bash

PROJECT_DT1=$(date +%Y%m%d)
PROJECT_DT2=$(date +%Y%m%d%H%m)
PROJECT_DT=$(date +%Y%m%d%H%m)

prodir=$(pwd)

cd LA.UM.5.6/LINUX/android/

if [ $TARGET_BUILD_VARIANT = "user" ];then
	if [ $PLATFORM_ENABLE_ADB = "true" ];then
	    sw_dir=${prodir}/SW_DIR
	else
		sw_dir=${prodir}/SW_DIR
	fi
else
	sw_dir=${prodir}/SW_DIR
fi

PLATFORM_PRODUCT_MODEL=${TARGET_PRODUCT}

if [ $TARGET_BUILD_VARIANT = "user" ];then
	if [ $PLATFORM_ENABLE_ADB = "true" ];then
	    sw_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_${TARGET_BUILD_VARIANT}_adb_${PROJECT_DT}
	    ota_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_ota_${TARGET_BUILD_VARIANT}_adb_${PROJECT_DT}
	    target_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_target_${TARGET_BUILD_VARIANT}_adb_${PROJECT_DT}
	else
		sw_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_${TARGET_BUILD_VARIANT}_${PROJECT_DT}
		ota_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_ota_${TARGET_BUILD_VARIANT}_${PROJECT_DT}
		target_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_target_${TARGET_BUILD_VARIANT}_${PROJECT_DT}
	fi
else
	sw_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_${TARGET_BUILD_VARIANT}_${PROJECT_DT}
	ota_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_ota_${TARGET_BUILD_VARIANT}_${PROJECT_DT}
	target_file=${PLATFORM_PRODUCT_MODEL}_${TARGET_VERSION}_target_${TARGET_BUILD_VARIANT}_${PROJECT_DT}
fi

outdir=${sw_dir}/${sw_file}
otadir=${sw_dir}/${ota_file}
targetdir=${sw_dir}/${target_file}

# cp logo
cp device/qcom/common/display/logo/splash.img $OUT
systemimgs=(boot.img system.img userdata.img recovery.img persist.img cache.img mdtp.img emmc_appsboot.mbn ztsdcard.img splash.img szzt.img)
isBuildOk=true
for i in ${systemimgs[@]}; do
	myfile=$OUT/$i
	if [ ! -f "$myfile" ]; then  
	  isBuildOk=false
	fi
done
if [ $isBuildOk = "true" ];then
	if [ ! -d "$sw_dir" ]; then
		mkdir -p $sw_dir
	fi
	if [ $TARGET_BUILD_VARIANT = "user" ];then
		if [ $PLATFORM_ENABLE_ADB = "true" ];then
			rm -f $sw_dir/*_adb_*.zip 
		else
			rm -f $sw_dir/*_user_2017*.zip 
		fi
	else
		rm -rf $sw_dir/*
	fi
	#rm -f ${targetdir}*.zip 
	mkdir -p $outdir

	for i in ${systemimgs[@]}; do
		tempfile=$OUT/$i
		if [ -f "$tempfile" ]; then  
			cp $tempfile $outdir/
		fi
	done
	#cp $OUT/${PRODUCT_DEV}-ota-eng.$(whoami).zip $outdir/${PLATFORM_PRODUCT_MODEL}_OTA_${PROJECT_DT}.zip
	#cp $OUT/obj/PACKAGING/target_files_intermediates/${PRODUCT_DEV}-target_files-eng.$(whoami).zip $outdir/../${PLATFORM_PRODUCT_MODEL}_ota_pack_${PROJECT_DT}.zip
	
	rm -f ${outdir}*.zip
	
	zip -rj  ${outdir}.zip $outdir/*
	
	#rcrValue=$(./mycksum ${outdir}.zip)
	#mv ${outdir}.zip ${outdir}_${rcrValue}.zip
	rm -rf $outdir

	temp=$OUT/${TARGET_PRODUCT}*ota*.zip
	cp $temp  ${otadir}.zip
	#rcrValue=$(./mycksum ${otadir}.zip)
	#mv ${otadir}.zip ${otadir}_${rcrValue}.zip

	cp $OUT/obj/PACKAGING/target_files_intermediates/${TARGET_PRODUCT}-target_files*.zip ${targetdir}.zip
	#rcrValue=$(./mycksum ${targetdir}.zip)
	#mv ${targetdir}.zip ${targetdir}_${rcrValue}.zip

	#release_note_file=$OUT/../../../../release_note.xlsx
	#if [ -f "$release_note_file" ]; then  
	#	cp $release_note_file $sw_dir/
	#fi
fi

mkdir -p $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/docs
mkdir -p $OUTPUT_DIR/ROM
mkdir -p $OUTPUT_DIR/OTA
mkdir -p $OUTPUT_DIR/FullPack
mkdir -p $OUTPUT_DIR/Zip

FLASHTOOL_NAME="$VENDOR_PRODUCT_MODEL.$TARGET_VERSION.zip"
mv ${outdir}.zip  $OUTPUT_DIR/Zip/$FLASHTOOL_NAME
OTA_TARGET_NAME="target_$FLASHTOOL_NAME"
mv ${targetdir}.zip $OUTPUT_DIR/Zip/$OTA_TARGET_NAME

FullPack_NAME="$VENDOR_PRODUCT_MODEL-$TARGET_VERSION-for-all-update.zip"
mv ${otadir}.zip $OUTPUT_DIR/FullPack/$FullPack_NAME

cd ${prodir}
