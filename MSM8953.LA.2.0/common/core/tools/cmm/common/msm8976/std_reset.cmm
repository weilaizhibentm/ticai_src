//============================================================================
//  Name:                                                                     
//    std_reset.cmm 
//
//  Description:                                                              
//		Top level reset script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who     		what, where, why
// --------   ---     		---------------------------------------------------------------------
// 07/13/2015 c_gunnan      Add support for 8976
// 04/24/2014 VijayJ		Add support for MSM8936
// 09/10/2013 AJCheriyan   	Disable PMIC watchdog added
// 05/25/2013 AJCheriyan    Removed chip revision checking during reset
// 02/12/2013 AJCheriyan	Rebased to 8974 version of script
// 02/11/2013 AJCheriyan	Pulled in DLOAD cookie clearing logic from HLOS reset scripts
// 02/08/2013 AJCheriyan	Added change to clear boot partition select / watchdog enable registers
// 10/10/2012 AJCheriyan	Added change to go to end of RPM boot loader to turn on IMEM clocks
// 08/13/2012 AJCheriyan	Manually reset the security block to workaround V1 HW bug
// 07/19/2012 AJCheriyan    Created for B-family (8974)
//

LOCAL &TZBSP_SHARED_IMEM_DUMP_MAGIC
LOCAL &image 
ENTRY &image

&TZBSP_SHARED_IMEM_DUMP_MAGIC=0x08600748
LOCAL &PWD

MAIN:

	// Check for any active sessions
	do std_intercom_init CHECKSESSION 4 RPM CLUSTER0_APPS0 MPSS WCNSS ADSP
	
	// "Reset" all T32 sessions except APPS
	IF (("&RPM_ALIVE"!="0.")&&("&RPM_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &RPM_PORT SYS.D
	)
	IF (("&CLUSTER1_APPS1_ALIVE"!="0.")&&("&CLUSTER1_APPS1_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &CLUSTER1_APPS1_PORT SYS.D
	)
	IF (("&CLUSTER1_APPS2_ALIVE"!="0.")&&("&CLUSTER1_APPS2_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &CLUSTER1_APPS2_PORT SYS.D
	)
	IF (("&CLUSTER1_APPS3_ALIVE"!="0.")&&("&CLUSTER1_APPS3_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &CLUSTER1_APPS3_PORT SYS.D
	)
	IF (("&CLUSTER0_APPS0_ALIVE"!="0.")&&("&CLUSTER0_APPS0_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &CLUSTER0_APPS0_PORT SYS.D
	)
	IF (("&CLUSTER0_APPS1_ALIVE"!="0.")&&("&CLUSTER0_APPS1_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &CLUSTER0_APPS1_PORT SYS.D
	)
	IF (("&CLUSTER0_APPS2_ALIVE"!="0.")&&("&CLUSTER0_APPS2_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &CLUSTER0_APPS2_PORT SYS.D
	)
	IF (("&CLUSTER0_APPS3_ALIVE"!="0.")&&("&CLUSTER0_APPS3_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &CLUSTER0_APPS3_PORT SYS.D
	)
	IF (("&MPSS_SCALAR_ALIVE"!="0.")&&("&MPSS_SCALAR_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &MPSS_SCALAR_PORT SYS.D
	)
	IF (("&MPSS_VECTOR_ALIVE"!="0.")&&("&MPSS_VECTOR_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &MPSS_VECTOR_PORT SYS.D
	)
	IF (("&WCNSS_ALIVE"!="0.")&&("&WCNSS_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &WCNSS_PORT SYS.D
	)
	IF (("&ADSP_ALIVE"!="0.")&&("&ADSP_ALIVE"!=""))
	(
		INTERCOM.EXECUTE &ADSP_PORT SYS.D
	)
	
	IF ((("&CLUSTER0_APPS0_ALIVE"!="0.")&&("&CLUSTER0_APPS0_ALIVE"!=""))||("&LOCALHOST"=="&CLUSTER0_APPS0_PORT"))
	(
		 SYS.UP
	
		// Load HWIO
		do hwio
		//Configure SPMI
		do spmiCfg.cmm 8976 true
		
		IF (("&image"=="mpss")||("&image"=="adsp")||("&image"=="mba")||("&image"=="wcnss")||("&image"=="rpm"))
		(
			do pmicpeek.cmm 0x85b 0x0 0x0
			wait 1s
			do pmicpeek.cmm 0x85a 0x0 0x8
			do pmicpeek.cmm 0x85b 0x0 0x80
			wait 1s
			d.set a:0x4ab000 %le %long 0x0
			wait 100ms
			sys.m.a
			b
		)
		ELSE
		(
			do pmicpeek.cmm 0x14746 0x0 0x80
		)
		
		// Find version of the chipset
		//do std_platinfo
		
		//Clear debug registers and Dload config register
		do std_utils HWIO_OUT TCSR_BOOT_MISC_DETECT 0x0
		do std_utils HWIO_OUT TCSR_RESET_DEBUG_SW_ENTRY 0x0
		do std_utils HWIO_OUT GCC_RESET_DEBUG 0x400000
		do std_utils HWIO_OUT GCC_RESET_STATUS 0x0
		
		//Enable the Global MPM timer
		do std_utils HWIO_OUT MPM2_MPM_CONTROL_CNTCR 0x1
		
		TrOnchip.Set RESET OFF
		SYSTEM.option SYSPWRUPREQ OFF
		SYSTEM.option PWRDWN ON
		d.s EAXI:0x6001FB0 %LE %LONG 0xc5acce55
		d.s EAXI:0x6001060 %LE %LONG 0x2
		d.s EAXI:0x6001FB0 %LE %LONG 0xa5a5a5a5 
		DATA.SET EAXI:&TZBSP_SHARED_IMEM_DUMP_MAGIC %LONG %LE 0x00000000
		
		// Call the HLOS specific reset script
		//&HLOSCRIPT=STR.LWR("&HLOS")+"/std_reset_"+STR.LWR("&HLOS")      
		&HLOSCRIPT="std_reset_"+STR.LWR("&HLOS")      
		do &HLOSCRIPT
	)
	
	GOTO EXIT



FATALEXIT:
	END

EXIT:
	ENDDO



