//============================================================================
//  Name:                                                                     
//    std_debug_msm8994.cmm 
//
//  Description:                                                              
//    Top level debug script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 08/05/2015 JBILLING      Added APPSBOOT_BKPT subroutine
// 07/13/2015 c_gunnan      Created for 8976
// 06/15/2015 JBILLING      Fixes for std_debug_rpm
// 05/11/2015 JBILLING      Fixes for command line parsing
// 01/31/2015 JBILLING      8996 additions, SLPI
// 05/22/2014 JBILLING      Added fields for all subprocessors
// 05/20/2014 JBilling      Created for 8994 and beyond. Now is a target-specific library file


// Following arguments are supported. 
// &UTILITY - subroutine to call. GETDEBUGDEFAULTS and SETDEBUGCOOKIE supported. Otherwise exit
// &OPTION - 
// ARG0 - Image to debug. 
// ARG1 - En/Disable Sleep. lpm_enable / lpm_disable
// ARG2 - Entry point for this debug session. will default to image entry point
//        if nothing is specified.


LOCAL &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
ENTRY &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11

//    Locals: Local Variables
//    Input Arguments
//LOCAL &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4

//    Return values to the caller along with success / fail value specific to utility
LOCAL &PASS &RVAL0 &RVAL1 &RVAL2

// Name of the utility we are calling
LOCAL &SUBROUTINE

// Any subroutine specific options
// Default exists for each subroutine

// Input Argument 0 is the name of the utility
&SUBROUTINE="&UTILITY"

IF !(("&SUBROUTINE"=="GETDEBUGDEFAULTS")||("&SUBROUTINE"=="SETDEBUGCOOKIE")||("&SUBROUTINE"=="APPSBOOT_BKPT"))
(
    PRINT %ERROR "WARNING: UTILITY &UTILITY DOES NOT EXIST."
    GOTO EXIT
)
ELSE
(
    // This should be created by some top level script. The setupenv for each proc would
    // set this up
     AREA.SELECT

     // Debug Print Statement
    // PRINT "&SUBROUTINE &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG5 &IARG6 &IARG7"
    // Call the required utility
    GOSUB &SUBROUTINE &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
    ENTRY &PASS &RVAL0 &RVAL1 &RVAL2

    GOTO EXIT
    
)

////////////////////////////////////////////
//        Name: APPSBOOT_BKPT
//        Function: Sets or clears debug cookie for desired subsystem
//        Inputs: 
//              &ARG0 - image to set debug cookie for
//              &ACTION - "clear" - clears debug cookie. Else Sets it.
//
/////////////////////////////////////////////
APPSBOOT_BKPT:
    LOCAL &set_command
    ENTRY &set_command

    LOCAL &AppsBootBkpt
    
    IF ("&HLOS"=="WP")
    (
        &AppsBootBkpt=0x80200000
    )
    ELSE 
    (
        &AppsBootBkpt=0x8F600000
    )
    
    IF "&set_command"=="remove"
    (
        PRINT "Removing apps breakpoints"
        B.DELETE &AppsBootBkpt
        RETURN
    )
    ELSE
    (
        PRINT "Disabling other breakpoints, setting appsboot entry breakpoint"
        BREAK.DISABLE /ALL

        B.DELETE &AppsBootBkpt
        B.S &AppsBootBkpt /o     //appsboot entry
    )
    
    
    GO
    &counter=0
    LOCAL &counter2 &exitflag
    &counter2=0
    &exitflag=0
    PRINT "Waiting to get to appsboot. Please Wait..."
    WHILE (STATE.RUN()&&(&counter<6000.)&&(&exitflag==0))
    (
        WAIT.100ms
        &counter=&counter+1
        &counter2=&counter2+1
        
        if &counter2>5.
        (
            &counter2=0
            //Break processor, get PC
            BREAK
            &r=register(pc)

            if (&r&0xFFF0000000000000)!=0x0
            (
                &exitflag=1.
            )
            //If we're not in HLOS yet, GO. if we are, keep broken.
            else
            (
                GO
            )
        
        )
    )
    IF &counter==6000.
    (
        PRINT "Error - apps could not stop. Please check if USB was connected across reset"
    )
    
    B.DELETE &AppsBootBkpt
    
RETURN
////////////////////////////////////////////
//        Name: SETDEBUGCOOKIE
//        Function: Sets or clears debug cookie for desired subsystem
//        Inputs: 
//              &ARG0 - image to set debug cookie for
//              &ACTION - "clear" - clears debug cookie. Else Sets it.
//
/////////////////////////////////////////////

SETDEBUGCOOKIE:
    LOCAL &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9 &ARG10 &ARG11
    ENTRY &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9 &ARG10 &ARG11

    LOCAL &DebugCookieAddress
    &Loc_ARG0=string.lwr("&ARG0")
    //&Action should be "None"
    
          //############RPM DEBUG#############        
            IF ("&Loc_ARG0"=="rpm")
            (
                &DebugCookieAddress=&RPM_DEBUG_COOKIE
            )
            //############APPS PROCESSOR DEBUG#############
            ELSE IF ("&Loc_ARG0"=="appspbl")
            (
                RETURN
            )
            ELSE IF ("&Loc_ARG0"=="sbl")
            (
                &DebugCookieAddress=&SBL1_DEBUG_COOKIE
            )
            ELSE IF ("&Loc_ARG0"=="tz")
            (
                &DebugCookieAddress=&SBL1_DEBUG_COOKIE
            )
            ELSE IF ("&Loc_ARG0"=="appsboot")
            (
                &DebugCookieAddress=&SBL1_DEBUG_COOKIE
            )
            
            //############MODEM_Q6 DEBUG#############                    
            ELSE IF ("&Loc_ARG0"=="mpss")
            (
                &DebugCookieAddress=&MPSS_DEBUG_COOKIE
                do std_utils HWIO_OUTF GCC_MSS_CFG_AHB_CBCR CLK_ENABLE 0x1 EZAXI
                wait.200ms
                do std_utils HWIO_OUTF MSS_QDSP6SS_DBG_CFG DBG_SW_REG 0x20


            )
            ELSE IF ("&Loc_ARG0"=="mba")
            (
                &DebugCookieAddress=&MBA_DEBUG_COOKIE

                do std_utils HWIO_OUTF GCC_MSS_CFG_AHB_CBCR CLK_ENABLE 0x1 EZAXI
                wait.200ms
                do std_utils HWIO_OUTF MSS_QDSP6SS_DBG_CFG DBG_SW_REG 0x10
            )

            //############ADSP_Q6 DEBUG#############                                
            ELSE IF (("&Loc_ARG0"=="adsp"))
            (
                &DebugCookieAddress=&ADSP_DEBUG_COOKIE
                BREAK
                
                    
                do std_utils HWIO_OUTF GCC_LPASS_SWAY_CBCR CLK_ENABLE 1
                do std_utils HWIO_OUTF LPASS_Q6SS_AHB_LFABIF_CBCR CLK_ENABLE 0x1
                do std_utils HWIO_OUTF LPASS_QDSP6SS_XO_CBCR CLKEN 0x1
                wait.200ms
                do std_utils HWIO_OUTF LPASS_QDSP6SS_DBG_CFG DBG_SW_REG 0x20 
                
            )
          
            //############WCNSS_ARM9 DEBUG#############                                
            ELSE IF (("&Loc_ARG0"=="wcnss"))
            (
                &DebugCookieAddress=&WCNSS_DEBUG_COOKIE
            )
            ELSE
            (
                print %error "Error! Unknown image: &ARG0 specified"
                GOTO FATALEXIT
            )
            
            //set the value
            IF STR.LWR("&ACTION")!="clear"
            (
                DATA.SET EA:&DebugCookieAddress %LONG %LE &DEBUG_COOKIE_VALUE
            )
            ELSE //user wants cookie cleared
            (
                DATA.SET EA:&DebugCookieAddress %LONG %LE 0x0 
            )
            
            
    RETURN //SETDEBUGCOOKIE. 
  

////////////////////////////////////////////
//            Function: GETDEBUGDEFAULTS
//
//
//               &rvalue_targetprocessor="RPM" -> 
//                      Multiple images may map to same processor (e.g. MBA and MPSS on the modem Q6)
//               &rvalue_symbolloadscript="std_loadsyms_rpm"
//                      Specifies the load script that std_debug will call to load symbols on your target processor
//               &rvalue_buildpath="&RPM_BUILDROOT"
//                      The Build Path for your image's symbols.
//               &rvalue_debugscript="std_debug_rpm" 
//                      Debug script for your image, which std_debug will call once the target image is in initialization.
//               &rvalue_peripheral_processor="FALSE"
//                      Indicates if the target is a peripheral processor, in which case CTI is used to communicate with the boot processor that it initialized.
//                      Currently only Q6's use this.
//               &rvalue_targetprocessorport=&RPM_PORT
//                      The JTag intercom port to execute intercom commands on the remote JTag window
//               &rvalue_processortimeoutvalue=0x1
//                      Once the system starts booting up, it will wait for either a CTI signal or timeout, then JTag attempts to break.
//                      Q6's take a while to load, depending on how long apps takes to load them (say 20 seconds), whereas RPM and bootchain are nearly immediate
//                      timeout value is in increments of 100 milliseconds.
//                      
//
////////////////////////////////////////////
GETDEBUGDEFAULTS:
    LOCAL &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9 &ARG10 &ARG11 &ARG12
    ENTRY &ACTION &ARG0 &ARG1 &ARG2 &ARG3 &ARG4 &ARG5 &ARG6 &ARG7 &ARG8 &ARG9 &ARG10 &ARG11 &ARG12
        
            //locals for given parameters
            LOCAL &given_image &given_lpm_option &given_cti_enable &given_alternateelf &given_extraoption
            
            
            &given_image=string.lwr("&ARG0")
            &given_lpm_option=string.lwr("&ARG1")
            &given_cti_enable=string.lwr("&ARG2")
            &given_alternateelf=string.lwr("&ARG3")
            &given_extraoption="&ARG4"

            //return value variables
            LOCAL &rvalue_image &rvalue_targetprocessor &rvalue_targetprocessorport &rvalue_bootprocessor &rvalue_bootprocessorport &rvalue_peripheral_processor &rvalue_processortimeoutvalue &rvalue_debugscript &rvalue_symbolloadscript &rvalue_buildpath &rvalue_entry_bkpt &rvalue_error_bkpts &rvalue_lpm_option &rvalue_sleep_disable_command &rvalue_cti_enable &rvalue_multi_elf_option &rvalue_alternateelf &rvalue_extraoption

                    

            
            &rvalue_image="&given_image"
            
            
            //For simulator, intercom ports are not initialized.
            IF (STRING.SCAN("&RPM_PORT","P",0)!=-1)
            (
                GLOBAL &RPM_PORT &MPSS_SCALAR_PORT &MPSS_VECTOR_PORT &ADSP_PORT &WCNSS_PORT &CLUSTER1_APPS0_PORT
                
                &RPM_PORT=0x0
                &MPSS_SCALAR_PORT=0x0
                &MPSS_VECTOR_PORT=0x0
                &ADSP_PORT=0x0
                &WCNSS_PORT=0x0
                &CLUSTER1_APPS0_PORT=0x0
            )
            


            
            IF ("&LOCALHOST"=="&CLUSTER1_APPS0_PORT")
            (
                &rvalue_bootprocessor="CLUSTER1_APPS0"
                &rvalue_bootprocessorport="&CLUSTER1_APPS0_PORT"
            )
            ELSE
            (
                &rvalue_bootprocessor="CLUSTER1_APPS0"
                &rvalue_bootprocessorport="&CLUSTER1_APPS0_PORT"
            )
            
            //FIXME - MBA debug needs verification
            //############RPM DEBUG#############        
            IF ("&given_image"=="rpm")
            (
               &rvalue_targetprocessor="RPM"
               &rvalue_targetprocessorport=&RPM_PORT
               &rvalue_peripheral_processor="false"
               &rvalue_processortimeoutvalue=0x1
               &rvalue_debugscript="std_debug_rpm"
               &rvalue_symbolloadscript="std_loadsyms_rpm"
               &rvalue_buildpath="&RPM_BUILDROOT"
               &rvalue_entry_bkpt="exceptions_init"
               &rvalue_error_bkpts="abort"
               &rvalue_sleep_disable_command="v sleep_allow_low_power_modes=0"
               
               //defaults. rvalues are modified below depending on user input.
               &rvalue_lpm_option="lpm_enable"
               &rvalue_cti_enable="false"
               &rvalue_multi_elf_option="false"
            )

            
            //############APPS PROCESSOR DEBUG#############
            ELSE IF ("&given_image"=="appspbl")
            (
                IF ("&LOCALHOST"=="&CLUSTER1_APPS0_PORT")
                (
                  &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )
                ELSE
                (
                    &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )

                &rvalue_peripheral_processor="false"
                &rvalue_processortimeoutvalue=0x14
                &rvalue_debugscript="std_debug_boot"
                &rvalue_symbolloadscript="NULL"
                &rvalue_buildpath="&BOOT_BUILDROOT"
                &rvalue_entry_bkpt="NULL"
                &rvalue_error_bkpts="NULL"
                &rvalue_sleep_disable_command="NULL"

                //defaults. rvalues are modified below depending on user input.
                &rvalue_lpm_option="lpm_enable"
                &rvalue_cti_enable="false"
                &rvalue_multi_elf_option="false"
            )
            ELSE IF ("&given_image"=="sbl")
            (
                IF ("&LOCALHOST"=="&CLUSTER1_APPS0_PORT")
                (
                   &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )
                ELSE
                (
                  &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )
                &rvalue_peripheral_processor="false"
                &rvalue_processortimeoutvalue=0x14
                &rvalue_debugscript="std_debug_boot"
                &rvalue_symbolloadscript="std_loadsyms_sbl"
                &rvalue_buildpath="&BOOT_BUILDROOT"
                &rvalue_entry_bkpt="sbl1_main_ctl"
                &rvalue_error_bkpts="boot_error_handler"
                &rvalue_sleep_disable_command="NULL"

                //defaults. rvalues are modified below depending on user input.
                &rvalue_lpm_option="lpm_enable"
                &rvalue_cti_enable="false"
                &rvalue_multi_elf_option="false"
            )
            ELSE IF ("&given_image"=="tz")
            (
                 IF ("&LOCALHOST"=="&CLUSTER1_APPS0_PORT")
                (
                   &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )
                ELSE
                (
                  &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )
                &rvalue_peripheral_processor="false"
                &rvalue_processortimeoutvalue=0x20
                &rvalue_debugscript="std_debug_tz"
                &rvalue_symbolloadscript="std_loadsyms_tz"
                &rvalue_buildpath="&TZ_BUILDROOT"
                &rvalue_entry_bkpt="tzbsp_entry_handler"
                &rvalue_error_bkpts="tzbsp_err_fatal"
                &rvalue_sleep_disable_command="NULL"

                //defaults. rvalues are modified below depending on user input.
                &rvalue_lpm_option="lpm_enable"
                &rvalue_cti_enable="false"
                &rvalue_multi_elf_option="false"
            )
            ELSE IF ("&given_image"=="appsboot")
            (
                IF ("&LOCALHOST"=="&CLUSTER1_APPS0_PORT")
                (
                 &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )
                ELSE
                (
              &rvalue_targetprocessor="CLUSTER1_APPS0"
                &rvalue_targetprocessorport="&CLUSTER1_APPS0_PORT"
                )

                &rvalue_peripheral_processor="false"
                &rvalue_processortimeoutvalue=0x20
                &rvalue_debugscript="std_debug_&HLOS"
                &rvalue_symbolloadscript="std_loadsyms_apps"
                &rvalue_buildpath="&APPS_BUILDROOT"
                &rvalue_entry_bkpt="0x8F600000"
                &rvalue_error_bkpts="0xFFFFFFFF"
                &rvalue_sleep_disable_command=""

                //defaults. rvalues are modified below depending on user input.
                &rvalue_lpm_option="lpm_enable"
                &rvalue_cti_enable="false"
                &rvalue_multi_elf_option="false"
            )
            ELSE IF ("&given_image"=="ipa")
            (
               &rvalue_targetprocessor="ipa"
               &rvalue_targetprocessorport="NULL"
               &rvalue_peripheral_processor="true"
               &rvalue_processortimeoutvalue=0x928
               &rvalue_debugscript="NULL"
               &rvalue_symbolloadscript="std_loadsyms_ipa"
               &rvalue_buildpath="&MPSS_BUILDROOT"
               &rvalue_entry_bkpt="main"
               &rvalue_error_bkpts="NULL"
               &rvalue_sleep_disable_command="NULL"
               
               //defaults. rvalues are modified below depending on user input.
               &rvalue_lpm_option="lpm_enable"
               &rvalue_cti_enable="false"
               &rvalue_multi_elf_option="false"
            )            
            //############MODEM_Q6 DEBUG#############                    
            ELSE IF ("&given_image"=="mpss")
            (
               &rvalue_targetprocessor="MPSS_SCALAR"
               &rvalue_targetprocessorport="&MPSS_SCALAR_PORT"
               &rvalue_peripheral_processor="true"
               &rvalue_processortimeoutvalue=0x928
               &rvalue_debugscript="std_debug_mpss"
               &rvalue_symbolloadscript="std_loadsyms_mpss"
               &rvalue_buildpath="&MPSS_BUILDROOT"
               &rvalue_entry_bkpt="main"
               &rvalue_error_bkpts="QURTK_handle_error,QURTK_tlb_crash,QURTK_error,QURTK_user_fatal_exit,QURTK_fatal_hook,err_fatal_lock,err_fatal_handler,err_SaveFatal3,QURTK_handle_nmi,QURTK_handle_nmi_local"
               &rvalue_sleep_disable_command="v g_sleepAllowLowPowerModes=0"
               
               //defaults. rvalues are modified below depending on user input.
               &rvalue_lpm_option="lpm_enable"
               &rvalue_cti_enable="false"
               &rvalue_multi_elf_option="false"
            )
            ELSE IF ("&given_image"=="mba") 
            (
               &rvalue_targetprocessor="MPSS_SCALAR"
               &rvalue_targetprocessorport="&MPSS_SCALAR_PORT"
               &rvalue_peripheral_processor="true"
               &rvalue_processortimeoutvalue=0x928
               &rvalue_debugscript="std_debug_mpss"
               &rvalue_symbolloadscript="std_loadsyms_mpss"
               &rvalue_buildpath="&MPSS_BUILDROOT"  //FIXME - std_loadsyms should be smarter, and could point directly to build.
               &rvalue_entry_bkpt="main"
               &rvalue_error_bkpts="QURTK_handle_error,QURTK_tlb_crash,QURTK_error,QURTK_user_fatal_exit,QURTK_fatal_hook,err_fatal_lock,err_fatal_handler,err_SaveFatal3,QURTK_handle_nmi,QURTK_handle_nmi_local"
               &rvalue_sleep_disable_command="NULL"
               
               //defaults. rvalues are modified below depending on user input.
               &rvalue_lpm_option="lpm_enable"
               &rvalue_cti_enable="false"
               &rvalue_multi_elf_option="false"
            )

            
            //############ADSP_Q6 DEBUG#############    
            ELSE IF (("&given_image"=="adsp"))
            (
               &rvalue_targetprocessor="adsp"
               &rvalue_targetprocessorport="&ADSP_PORT"
               &rvalue_peripheral_processor="true"
               &rvalue_processortimeoutvalue=0x928
               &rvalue_debugscript="std_debug_adsp"
               &rvalue_symbolloadscript="std_loadsyms_adsp"
               &rvalue_buildpath="&ADSP_BUILDROOT"
               &rvalue_entry_bkpt="main"
               &rvalue_error_bkpts="QURTK_handle_error,QURTK_tlb_crash,QURTK_error,QURTK_user_fatal_exit,QURTK_fatal_hook,err_fatal_lock,err_fatal_handler,err_SaveFatal3,QURTK_handle_nmi,QURTK_handle_nmi_local"
               &rvalue_sleep_disable_command="v g_sleepAllowLowPowerModes=0"
               
               //defaults. rvalues are modified below depending on user input.
               &rvalue_lpm_option="lpm_enable"
               &rvalue_cti_enable="false"
               &rvalue_multi_elf_option="true"
            )            
            ELSE IF (("&given_image"=="wcnss")) 
            (
               &rvalue_targetprocessor="wcnss"
               &rvalue_targetprocessorport="&WCNSS_PORT"
               &rvalue_peripheral_processor="true"
               &rvalue_processortimeoutvalue=0x928
               &rvalue_debugscript="std_debug_wcnss"
               &rvalue_symbolloadscript="std_loadsyms_wcnss"
               &rvalue_buildpath="&WCNSS_BUILDROOT"
               &rvalue_entry_bkpt="boot_main_ctl"
               &rvalue_error_bkpts="NULL"
               &rvalue_sleep_disable_command="v sleep_allow_low_power_modes=0"
               
               //defaults. rvalues are modified below depending on user input.
               &rvalue_lpm_option="lpm_enable"
               &rvalue_cti_enable="false"
               &rvalue_multi_elf_option="false"
               
            )
            ELSE
            (
                PRINT %ERROR "Error! Unknown image: &ARG0 specified"
                GOTO FATALEXIT
            )
        
        
            IF ("&given_lpm_option"=="lpm_disable")
            (
                &rvalue_lpm_option="lpm_disable"
            )
            IF ("&given_lpm_option"=="lpm_enable")
            (
                &rvalue_lpm_option="lpm_enable"
            )

            IF ("&given_cti_enable"=="true")
            (
                &rvalue_cti_enable="true"
            )
            IF ("&given_cti_enable"=="false")
            (
                &rvalue_cti_enable="false"
            )

            IF ("&given_alternateelf"=="")
            (
                &rvalue_alternateelf="NULL"
            )
            ELSE
            (
                &rvalue_alternateelf="&given_alternateelf"
            )

            
            //Just pass extraoption through
            &rvalue_extraoption="&given_extraoption"
        
        
            ENDDO &rvalue_image &rvalue_targetprocessor &rvalue_targetprocessorport &rvalue_bootprocessor &rvalue_bootprocessorport &rvalue_peripheral_processor &rvalue_processortimeoutvalue &rvalue_debugscript &rvalue_symbolloadscript &rvalue_buildpath &rvalue_entry_bkpt &rvalue_error_bkpts &rvalue_lpm_option "&rvalue_sleep_disable_command" &rvalue_cti_enable &rvalue_multi_elf_option &rvalue_alternateelf &rvalue_extraoption
        
        
        
    
    
    
    RETURN //GETDEBUGDEFAULTS
EXIT:
    ENDDO

FATALEXIT:
    END







 
