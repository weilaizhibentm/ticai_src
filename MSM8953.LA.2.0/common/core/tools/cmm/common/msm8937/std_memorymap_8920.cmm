//   Title: std_memorymap

//   License: License
//   Copyright 2012 Qualcomm Technologies Inc

//   Description: This script setup the memory map for the target

//   Input: None

//   Output: None

//   Usage: do std_memorymap

//   Team: CoreBSP Products

//   Target: MSM8920

//   Author: Author
//   $Author: pwbldsvc $

//   Location: Perforce Revision
//   $Header: 
//
//   Edits : 		Edits
//   c_sknven : 	Created new memory map file for 8920	: 04/21/2016

ENTRY &ARGUMENT0

LOCAL &IMAGE_TYPE
&IMAGE_TYPE="&ARGUMENT0"

GLOBAL &LK_ENTRY_ADDR
&LK_ENTRY_ADDR=0x8f600000 // lk entry address
GLOBAL  &PCNOC_OBS_FAULTEN_ADDR   // pcnoc address used to disable pcnoc errors while debugging
&PCNOC_OBS_FAULTEN_ADDR=0x500008
GLOBAL  &SNOC_OBS_FAULTEN_ADDR        //snoc address used to diable snoc errors while debugging 
&SNOC_OBS_FAULTEN_ADDR=0x580008

// ADSP EFS Image - 128 KB
GLOBAL &ADSP_EFS_start
GLOBAL &ADSP_EFS_end
GLOBAL &ADSP_EFS_size
GLOBAL &ADSP_EFS_log
&ADSP_EFS_start=0x867E0000 //RFSA BUFFER_START
&ADSP_EFS_end=0x86800000   //
&ADSP_EFS_size=0X20000
&ADSP_EFS_log="ADSP_EFS_log.bin"

// ADSP SW Image - 17MB
GLOBAL &ADSP_SW_start
GLOBAL &ADSP_SW_end
GLOBAL &ADSP_SW_size
GLOBAL &ADSP_SW_log
&ADSP_SW_start=0x8D200000
&ADSP_SW_end=0x8E300000
&ADSP_SW_size=0x1100000
&ADSP_SW_log="ADSP_SW_log.bin"

// RPM Code RAM - 144 KB
GLOBAL &CODERAM_start
GLOBAL &CODERAM_RPM_start
GLOBAL &CODERAM_end
GLOBAL &CODERAM_size
GLOBAL &CODERAM_log
GLOBAL &CODERAM_USB_log
&CODERAM_start=0x200000
&CODERAM_RPM_start=0x000000
&CODERAM_end=0x223FFF
&CODERAM_size=0X24000
&CODERAM_log="CODERAM_log.bin"
&CODERAM_USB_log="CODERAM.bin"

// RPM Data RAM - 64 kB 
GLOBAL &DATARAM_start
GLOBAL &DATARAM_RPM_start
GLOBAL &DATARAM_end
GLOBAL &DATARAM_size
GLOBAL &DATARAM_log
GLOBAL &DATARAM_USB_log
&DATARAM_start=0x290000
&DATARAM_RPM_start=0x90000
&DATARAM_end=0x29FFFF
&DATARAM_size=0X10000
&DATARAM_log="DATARAM_log.bin"
&DATARAM_USB_log="DATARAM.bin"


// RPM Message RAM - 20KB
GLOBAL &MSGRAM_start
GLOBAL &MSGRAM_end
GLOBAL &MSGRAM_size
GLOBAL &MSGRAM_log
GLOBAL &MSGRAM_USB_log
&MSGRAM_start=0x00060000
&MSGRAM_end=0x00065000
&MSGRAM_size=0x5000
&MSGRAM_log="MSGRAM_log.bin"
&MSGRAM_USB_log="MSGRAM.bin"


GLOBAL &HLOS_1_start
GLOBAL &HLOS_1_end
GLOBAL &HLOS_1_size
GLOBAL &HLOS_1_log
&HLOS_1_start=0X80000000
&HLOS_1_end=0x85B00000
&HLOS_1_size=0X5B00000
&HLOS_1_log="HLOS_1_log.bin"


GLOBAL &HLOS_2_start
GLOBAL &HLOS_2_end
GLOBAL &HLOS_2_size
GLOBAL &HLOS_2_log
&HLOS_2_start=0x8EA00000
&HLOS_2_end=0xA0000000
&HLOS_2_size=0x11600000
&HLOS_2_log="HLOS_2_log.bin"


GLOBAL &HLOS_3_start
GLOBAL &HLOS_3_end
GLOBAL &HLOS_3_size
GLOBAL &HLOS_3_log
&HLOS_3_start=0xA0000000
&HLOS_3_end=0xC0000000
&HLOS_3_size=0x20000000
&HLOS_3_log="HLOS_3_log.bin"
//MBA is relocatable and hence the below address may change
GLOBAL &MBA_SW_start
GLOBAL &MBA_SW_end
GLOBAL &MBA_SW_size
GLOBAL &MBA_SW_log
&MBA_SW_start=0x86700000
&MBA_SW_end=0x867E0000
&MBA_SW_size=0xE0000
&MBA_SW_log="MBA_SW_log.bin"


GLOBAL &MPSS_EFS_start
GLOBAL &MPSS_EFS_end
GLOBAL &MPSS_EFS_size
GLOBAL &MPSS_EFS_log
&MPSS_EFS_start=0x86700000
&MPSS_EFS_end=0x867E0000
&MPSS_EFS_size=0xE0000
&MPSS_EFS_log="MPSS_EFS_log.bin"


// MPSS Image - 106 MB
GLOBAL &MPSS_SW_start
GLOBAL &MPSS_SW_end
GLOBAL &MPSS_SW_size
GLOBAL &MPSS_SW_log
&MPSS_SW_start=0x86800000
&MPSS_SW_end=0x8D200000
&MPSS_SW_size=0x6A00000
&MPSS_SW_log="MPSS_SW_log.bin"

GLOBAL &MPSS_TCM_start
GLOBAL &MPSS_TCM_end
GLOBAL &MPSS_TCM_size
GLOBAL &MPSS_TCM_log

&MPSS_TCM_start=0x4400000
&MPSS_TCM_end=0x047FFFFF

&MPSS_TCM_size=0x3FFFFF
&MPSS_TCM_log="MPSS_TCM_log.bin"

// QDSS Image - 16 MB
GLOBAL &QDSS_SW_start
GLOBAL &QDSS_SW_end
GLOBAL &QDSS_SW_size
GLOBAL &QDSS_SW_log
&QDSS_SW_start=0x9000000
&QDSS_SW_end=0x0A000000
&QDSS_SW_size=0x1000000
&QDSS_SW_log="QDSS_SW_log.bin"


GLOBAL &SMEM_start
GLOBAL &SMEM_end
GLOBAL &SMEM_size
GLOBAL &SMEM_log
&SMEM_start=0x86300000
&SMEM_end=0x86400000
&SMEM_size=0X100000
&SMEM_log="SMEM_log.bin"


GLOBAL &TZ_SW_start
GLOBAL &TZ_SW_end
GLOBAL &TZ_SW_size
GLOBAL &TZ_SW_log
&TZ_SW_start=0x86500000
&TZ_SW_end=0x86700000
&TZ_SW_size=0X200000
&TZ_SW_log="TZ_SW_log.bin"

// WCNSS Image - 7 MB
GLOBAL &WCNSS_SW_start
GLOBAL &WCNSS_SW_end
GLOBAL &WCNSS_SW_size
GLOBAL &WCNSS_SW_log
&WCNSS_SW_start=0x8E300000
&WCNSS_SW_end=0x8EA00000
&WCNSS_SW_size=0X700000
&WCNSS_SW_log="WCNSS_SW_log.bin"

// WCNSS uBSP Image - Part of the 7MB 
// main image when loaded. Runs out of CMEM
GLOBAL &WCNSS_CMEM_start
GLOBAL &WCNSS_CMEM_end
GLOBAL &WCNSS_CMEM_size
GLOBAL &WCNSS_CMEM_log
&WCNSS_CMEM_start=0xA280000
&WCNSS_CMEM_end=0x0A300000
&WCNSS_CMEM_size=0x80000
&WCNSS_CMEM_log="WCNSS_CMEM_log.bin"

// IPA MEMORIES AND REGISTER SPACES
//-----------------------------------
//TODO::Update IPA memory regions as per IPCAT.
GLOBAL &IPA_IRAM_log
GLOBAL &IPA_IRAM_IPA_start
GLOBAL &IPA_IRAM_start
GLOBAL &IPA_IRAM_end
GLOBAL &IPA_IRAM_size
&IPA_IRAM_log="IPA_IRAM.bin"
&IPA_IRAM_IPA_start=0x0
&IPA_IRAM_start=0x7950000
&IPA_IRAM_end=0x7953FFF
&IPA_IRAM_size=0x4000

GLOBAL &IPA_DRAM_log
GLOBAL &IPA_DRAM_IPA_start
GLOBAL &IPA_DRAM_start
GLOBAL &IPA_DRAM_end
GLOBAL &IPA_DRAM_size
&IPA_DRAM_log="IPA_DRAM.bin"
&IPA_DRAM_IPA_start=0x4000
&IPA_DRAM_start=0x7954000
&IPA_DRAM_end=0x7957eff 
&IPA_DRAM_size=0x3F00

GLOBAL &IPA_SRAM_log 
GLOBAL &IPA_SRAM_start
GLOBAL &IPA_SRAM_end
GLOBAL &IPA_SRAM_size
&IPA_SRAM_log="IPA_SRAM.bin" 
&IPA_SRAM_start=0x7945000
&IPA_SRAM_end=0x7946FFF
&IPA_SRAM_size=0x2000

GLOBAL &IPA_HRAM_log 
GLOBAL &IPA_HRAM_start
GLOBAL &IPA_HRAM_end
GLOBAL &IPA_HRAM_size
&IPA_HRAM_log="IPA_HRAM.bin" 
&IPA_HRAM_start=0x7949000
&IPA_HRAM_end=0x794BEFF
&IPA_HRAM_size=0x3000


GLOBAL &IPA_DICT_log 
GLOBAL &IPA_DICT_start
GLOBAL &IPA_DICT_end
GLOBAL &IPA_DICT_size
&IPA_DICT_log="IPA_DICT.bin" 
&IPA_DICT_start=0x794F000
&IPA_DICT_end=0x794FFFF
&IPA_DICT_size=0x1000

GLOBAL &IPA_MBOX_log
GLOBAL &IPA_MBOX_start
GLOBAL &IPA_MBOX_end
GLOBAL &IPA_MBOX_size
&IPA_MBOX_log="IPA_MBOX.bin"
&IPA_MBOX_start=0x7962000
&IPA_MBOX_end=0x79620FF
&IPA_MBOX_size=0x100

GLOBAL &IPA_REG1_log
GLOBAL &IPA_REG1_start
GLOBAL &IPA_REG1_end
GLOBAL &IPA_REG1_size
&IPA_REG1_log="IPA_REG1.bin"
&IPA_REG1_start=0x7904000
&IPA_REG1_end=0x792Afff
&IPA_REG1_size=0x27000

GLOBAL &IPA_REG2_log
GLOBAL &IPA_REG2_start
GLOBAL &IPA_REG2_end
GLOBAL &IPA_REG2_size
&IPA_REG2_log="IPA_REG2.bin"
&IPA_REG2_start=0x7940000
&IPA_REG2_end=0x7944fff
&IPA_REG2_size=0x5000

GLOBAL &IPA_REG3_log
GLOBAL &IPA_REG3_start
GLOBAL &IPA_REG3_end
GLOBAL &IPA_REG3_size
&IPA_REG3_log="IPA_REG3.bin"
&IPA_REG3_start=0x7960000
&IPA_REG3_end=0x7961FFF
&IPA_REG3_size=0x2000


// DDR memory - combined space used by all images
GLOBAL &DDR_1_start
GLOBAL &DDR_1_end
GLOBAL &DDR_1_size
GLOBAL &DDR_1_log
GLOBAL &DDR_1_USB_log
GLOBAL &3GB_DDR_1_start
GLOBAL &3GB_DDR_1_end
GLOBAL &3GB_DDR_1_size
&DDR_1_start=0x80000000
&DDR_1_end=0xC0000000
&DDR_1_size=0x40000000	
&3GB_DDR_1_start=0x20000000
&3GB_DDR_1_end=0x80000000
&3GB_DDR_1_size=0x60000000
&DDR_1_log="DDRCS0.BIN"
&DDR_1_USB_log="DDRCS0.BIN"

GLOBAL &DDR_2_start
GLOBAL &DDR_2_end
GLOBAL &DDR_2_size
GLOBAL &DDR_2_log
GLOBAL &DDR_2_USB_log
GLOBAL &3GB_DDR_2_start
GLOBAL &3GB_DDR_2_end
GLOBAL &3GB_DDR_2_size
&DDR_2_start=0xC0000000
&DDR_2_end=0x100000000
&DDR_2_size=0x40000000
&3GB_DDR_2_start=0x80000000
&3GB_DDR_2_end=0xE0000000
&3GB_DDR_2_size=0x60000000
&DDR_2_log="DDRCS1.BIN"	
&DDR_2_USB_log="DDRCS1.BIN"

// This region is in the Shared IMEM block
// These are the cookies used to debug any image
// Allocation varies from target to target
GLOBAL &DEBUG_REGION_START
GLOBAL &DEBUG_REGION_END
GLOBAL &DEBUG_REGION_SIZE
GLOBAL &DEBUG_COOKIE_VALUE
GLOBAL &SBL1_DEBUG_COOKIE
GLOBAL &MBA_DEBUG_COOKIE
GLOBAL &MPSS_DEBUG_COOKIE
GLOBAL &ADSP_DEBUG_COOKIE
GLOBAL &WCNSS_DEBUG_COOKIE
GLOBAL &APPSBOOT_DEBUG_COOKIE
GLOBAL &RPM_DEBUG_COOKIE
&DEBUG_COOKIE_VALUE=0x53444247
&DEBUG_REGION_START=0x8600934
&DEBUG_REGION_END=0x860094C
&DEBUG_REGION_SIZE=0x18
&MPSS_DEBUG_COOKIE=0x8600934
&MBA_DEBUG_COOKIE=0x8600938
&WCNSS_DEBUG_COOKIE=0x8600940
&ADSP_DEBUG_COOKIE=0x860093C
&SBL1_DEBUG_COOKIE=0x8600944
&APPSBOOT_DEBUG_COOKIE=0x8600948
&RPM_DEBUG_COOKIE=0x08600B18

// This cookie used to allow modem mba, modem pbl and err_ready timeouts to be disabled
GLOBAL &PIL_TIMEOUT_DISABLE_COOKIE
&PIL_TIMEOUT_DISABLE_COOKIE=0x860094C

// OCIMEM section
GLOBAL &OCIMEM_start
GLOBAL &OCIMEM_end
GLOBAL &OCIMEM_size
GLOBAL &OCIMEM_log
GLOBAL &OCIMEM_USB_log
&OCIMEM_start=0x8600000
&OCIMEM_end=0x08606000
&OCIMEM_size=0x6000
&OCIMEM_log="OCIMEM.bin"
&OCIMEM_USB_log="OCIMEM.bin"

// Shared IMEM section
GLOBAL &SHARED_IMEM_start
GLOBAL &SHARED_IMEM_end
GLOBAL &SHARED_IMEM_size
GLOBAL &SHARED_IMEM_log
GLOBAL &SHARED_IMEM_USB_log
&SHARED_IMEM_start=0x8600000
&SHARED_IMEM_end=&SHARED_IMEM_start+0xFFF
&SHARED_IMEM_size=0x1000
&SHARED_IMEM_log="SHARED_IMEM.bin"



// Relocatable Image support
GLOBAL &RELOCATION_DATA_start
GLOBAL &RELOCATION_DATA_end
GLOBAL &RELOCATION_DATA_size
&RELOCATION_DATA_start=&SHARED_IMEM_start+0x94C
&RELOCATION_DATA_end=&RELOCATION_DATA_start+0xC7
&RELOCATION_DATA_size=0xC8
// Each entry in the table is in the following format
// 8 bytes - image name
// 8 bytes - image start address
// 4 bytes - image size


GLOBAL &WCNSS_regs
GLOBAL &WCNSS_mmu
&WCNSS_regs="WCNSS_regs.cmm"
&WCNSS_mmu="WCNSS_mmu.cmm"

GLOBAL &RPM_regs
GLOBAL &RPM_mmu
&RPM_regs="RPM_regs.cmm"
&RPM_mmu="RPM_regs.cmm"

GLOBAL &MPSS_Thread0_regs
&MPSS_Thread0_regs="MPSS_Thread0_regs.cmm"
GLOBAL &MPSS_Thread1_regs
&MPSS_Thread1_regs="MPSS_Thread1_regs.cmm"
GLOBAL &MPSS_Thread2_regs
&MPSS_Thread2_regs="MPSS_Thread2_regs.cmm"

GLOBAL &ADSP_Thread0_regs
&ADSP_Thread0_regs="ADSP_Thread0_regs.cmm"
GLOBAL &ADSP_Thread1_regs
&ADSP_Thread1_regs="ADSP_Thread1_regs.cmm"
GLOBAL &ADSP_Thread2_regs
&ADSP_Thread2_regs="ADSP_Thread2_regs.cmm"

GLOBAL &APPS_Cluster1_Core0_regs
GLOBAL &APPS_Cluster1_Core0_mmu
&APPS_Cluster1_Core0_regs="APPS_Cluster1_Core0_regs.cmm"
&APPS_Cluster1_Core0_mmu="APPS_Cluster1_Core0_mmu.cmm"

GLOBAL &APPS_Cluster1_Core1_regs
GLOBAL &APPS_Cluster1_Core1_mmu
&APPS_Cluster1_Core1_regs="APPS_Cluster1_Core1_regs.cmm"
&APPS_Cluster1_Core1_mmu="APPS_Cluster1_Core1_mmu.cmm"

GLOBAL &APPS_Cluster1_Core2_regs
GLOBAL &APPS_Cluster1_Core2_mmu
&APPS_Cluster1_Core2_regs="APPS_Cluster1_Core2_regs.cmm"
&APPS_Cluster1_Core2_mmu="APPS_Cluster1_Core2_mmu.cmm"

GLOBAL &APPS_Cluster1_Core3_regs
GLOBAL &APPS_Cluster1_Core3_mmu
&APPS_Cluster1_Core3_regs="APPS_Cluster1_Core3_regs.cmm"
&APPS_Cluster1_Core3_mmu="APPS_Cluster1_Core3_mmu.cmm"

GLOBAL &APPS_Cluster0_Core0_regs
GLOBAL &APPS_Cluster0_Core0_mmu
&APPS_Cluster0_Core0_regs="APPS_Cluster0_Core0_regs.cmm"
&APPS_Cluster0_Core0_mmu="APPS_Cluster0_Core0_mmu.cmm"

GLOBAL &APPS_Cluster0_Core1_regs
GLOBAL &APPS_Cluster0_Core1_mmu
&APPS_Cluster0_Core1_regs="APPS_Cluster0_Core1_regs.cmm"
&APPS_Cluster0_Core1_mmu="APPS_Cluster0_Core1_mmu.cmm"

GLOBAL &APPS_Cluster0_Core2_regs
GLOBAL &APPS_Cluster0_Core2_mmu
&APPS_Cluster0_Core2_regs="APPS_Cluster0_Core2_regs.cmm"
&APPS_Cluster0_Core2_mmu="APPS_Cluster0_Core2_mmu.cmm"

GLOBAL &APPS_Cluster0_Core3_regs
GLOBAL &APPS_Cluster0_Core3_mmu
&APPS_Cluster0_Core3_regs="APPS_Cluster0_Core3_regs.cmm"
&APPS_Cluster0_Core3_mmu="APPS_Cluster0_Core3_mmu.cmm"



MAIN:

	 ENTRY &LoadOption
	  If ("&LoadOption"=="DYNAMIC")
    (
	IF (("&IMAGE_TYPE"=="RPM")||("&IMAGE_TYPE"=="rpm"))
	(
		print "Skipping relocation parsing"
		GOTO EXIT
	)
	// Change any defaults
	IF (!SIMULATOR())
	(
		IF (("&IMAGE_TYPE"=="WCNSS")||("&IMAGE_TYPE"=="wcnss"))
		(
			GOSUB PARSE_IMAGE_RELOCATION_WCNSS
		)
		ELSE
		(	
		// Get relocated image info again
		GOSUB PARSE_IMAGE_RELOCATION
		)
		// check this later
		&HLOS_2_start=&WCNSS_SW_end+1
		&HLOS_2_end=0XA0000000
		&HLOS_2_size=0x14A00000-&WCNSS_SW_size
	)
	ELSE
	(
		// Do both together on simulator
		GOSUB RELOC_SIMEM_N_PARSE_IMAGE_RELOC
	)

)
	GOTO EXIT

EXIT:
	ENDDO



// Function to parse relocated image data that is 
// saved in internal memory
// This function will be run once only unless a new 
// session has started
PARSE_IMAGE_RELOCATION:
	LOCAL &HEAD &TAIL &IMG_NAME &IMG_START &IMG_SIZE

	&HEAD=&RELOCATION_DATA_start
	&TAIL=&RELOCATION_DATA_end

	WHILE (&HEAD<=&TAIL)
	(
		// Null terminate the string
		DATA.SET EA:&HEAD+0x4 %LONG DATA.LONG(EA:&HEAD+0x4)&0xFFFFFF
	 	&IMG_NAME=DATA.STRING(EA:&HEAD)
		&IMG_START=DATA.LONG(EA:&HEAD+0xC)<<32.|DATA.LONG(EA:&HEAD+0x8)
		&IMG_SIZE=DATA.LONG(EA:&HEAD+0x10)
		IF ("&IMG_NAME"=="modem")
		(
			&MPSS_SW_start=&IMG_START
			&MPSS_SW_size=&IMG_SIZE
			&MPSS_SW_end=&IMG_START+&IMG_SIZE-1
		)
		IF ("&IMG_NAME"=="wcnss")
		(
			&WCNSS_SW_start=&IMG_START
			&WCNSS_SW_size=&IMG_SIZE
			&WCNSS_SW_end=&IMG_START+&IMG_SIZE-1
		)
		IF ("&IMG_NAME"=="adsp")
		(
			&ADSP_SW_start=&IMG_START
			&ADSP_SW_size=&IMG_SIZE
			&ADSP_SW_end=&IMG_START+&IMG_SIZE-1
		)
		// Move to the next entry
		&HEAD=&HEAD+0x14
	)
	RETURN
PARSE_IMAGE_RELOCATION_WCNSS:
	LOCAL &HEAD &TAIL &IMG_NAME &IMG_START &IMG_SIZE
	&HEAD=&RELOCATION_DATA_start
	&TAIL=&RELOCATION_DATA_end
	WHILE (&HEAD<=&TAIL)
	(
		// Null terminate the string
		DATA.SET EAXI:&HEAD+0x4 %LONG DATA.LONG(EAXI:&HEAD+0x4)&0xFFFFFF
	 	&IMG_NAME=DATA.STRING(EAXI:&HEAD)
		&IMG_START=DATA.LONG(EAXI:&HEAD+0xC)<<32.|DATA.LONG(EAXI:&HEAD+0x8)
		&IMG_SIZE=DATA.LONG(EAXI:&HEAD+0x10)

		IF ("&IMG_NAME"=="modem")
		(
			&MPSS_SW_start=&IMG_START
			&MPSS_SW_size=&IMG_SIZE
			&MPSS_SW_end=&IMG_START+&IMG_SIZE-1
		)

		IF ("&IMG_NAME"=="wcnss")
		(
			&WCNSS_SW_start=&IMG_START
			&WCNSS_SW_size=&IMG_SIZE
			&WCNSS_SW_end=&IMG_START+&IMG_SIZE-1
		)

		// Move to the next entry
		&HEAD=&HEAD+0x14
	)

	RETURN


// Function to parse relocated image data that is 
// saved in internal memory and identify shared IMEM location
// This function will be run once only unless a new debug
// session has started	
RELOC_SIMEM_N_PARSE_IMAGE_RELOC:
	LOCAL &HEAD &TAIL &IMG_NAME &IMG_START &IMG_SIZE 
	LOCAL &RELOCATED &COUNT

	// Init locals
	&RELOCATED=0
	&COUNT=0

	// Start with default location for shared IMEM
	WHILE ((&RELOCATED==0)&&(&COUNT<1))
	(
		&HEAD=&RELOCATION_DATA_start
		&TAIL=&RELOCATION_DATA_end
		WHILE (&HEAD<=&TAIL)
		(
			// Null terminate the string
			DATA.SET EA:&HEAD+0x4 %LONG DATA.LONG(EA:&HEAD+0x4)&0xFFFFFF
		 	&IMG_NAME=DATA.STRING(EA:&HEAD)
			&IMG_START=DATA.LONG(EA:&HEAD+0xC)<<32.|DATA.LONG(EA:&HEAD+0x8)
			&IMG_SIZE=DATA.LONG(EA:&HEAD+0x10)


			IF ("&IMG_NAME"=="modem")
			(
				&MPSS_SW_start=&IMG_START
				&MPSS_SW_size=&IMG_SIZE
				&MPSS_SW_end=&IMG_START+&IMG_SIZE-1
				&RELOCATED=1
			)
			IF ("&IMG_NAME"=="adsp")
			(
				&ADSP_SW_start=&IMG_START
				&ADSP_SW_size=&IMG_SIZE
				&ADSP_SW_end=&IMG_START+&IMG_SIZE-1
				&RELOCATED=1
			)
				
			IF ("&IMG_NAME"=="wcnss")
			(
				&WCNSS_SW_start=&IMG_START
				&WCNSS_SW_size=&IMG_SIZE
				&WCNSS_SW_end=&IMG_START+&IMG_SIZE-1
				&RELOCATED=1
			)

			// Move to the next entry
			&HEAD=&HEAD+0x14
		)

		// Increment count
		&COUNT=&COUNT+1

	)

	// If we reach this point without finding relocated data, it means 911 time !
	IF (&RELOCATED==0)
	(
		PRINT "Relocated data not found at : &RELOCATION_DATA_start"
	)
	ELSE
	(
		PRINT "Relocated data found at : &RELOCATION_DATA_start"
	)

	RETURN


	

