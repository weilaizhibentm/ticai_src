//============================================================================
//  Name:                                                                     
//    std_loadbuild.cmm 
//
//  Description:                                                              
//    Top level build loading script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who     		what, where, why
// --------   ---     		---------------------------------------------------------
// 08/30/2016 c_sknven		Added scripts loading Support for LE flavor
// 01/05/2016 c_akaki		Added Secondary Boot and System image loading support
// 11/17/2015 c_akaki		Added CPE and VIDEO binary flash support
// 07/13/2015 c_gunnan      Created for 8976
// 02/08/2012 AJCheriyan	Fixed issue with buttons
// 07/19/2012 AJCheriyan    Created for B-family 

// We support only one argument which is the chipset. Should not be used.
ENTRY &ARG0

// Global variables populated by the the build options utility
LOCAL &NEW_BOOTBUILD &NEW_TZBUILD &NEW_RPMBUILD &NEW_APPSBUILD &NEW_MPSSBUILD &NEW_ADSPBUILD &NEW_WCNSSBUILD &NEW_SDIBUILD &NEW_WINSECAPPBUILD &NEW_CPEBUILD &NEW_VIDEOBUILD
LOCAL &NEW_PDFLAVOR

LOCAL &HLOS_LOADBUILD &LOAD_OPT &LOAD_METHOD &LOAD_IMG &FLAVOR_OPT
&LOAD_IMG="NULL"

MAIN:
	&NEW_RPMBUILD=0.
	&NEW_BOOTBUILD=0.
	&NEW_TZBUILD=0.
	&NEW_APPSBUILD=0.
	&NEW_MPSSBUILD=0.
	&NEW_WCNSSBUILD=0.
	&NEW_PDFLAVOR=0.
	&NEW_ADSPBUILD=0.
	&NEW_WINSECAPPBUILD=0.
	&NEW_CPEBUILD=0.
	&NEW_VIDEOBUILD=0.
    
	// Create the dialog for the script
	GOSUB CREATEDIALOG

	// Wait for the person to do something
	STOP

LOADBUILD:
	// We will not return to this script after this point
	DIALOG.END
	
	// Call the utility to map all the builds to the other sessions
	GOSUB BUILDSYNCUP

	// Call the top level load scripts
	do std_loadbuild &LOAD_OPT &LOAD_IMG &LOAD_METHOD
	
	// Done. Now Exit
	GOTO EXIT
	

// Sub-routine to create the dialog
CREATEDIALOG:
	
	// Check if the window exists
	WINTOP BUILDOPTIONS
	IF FOUND()
       RETURN 
	WINPOS ,,,,,, LOADSIM
	
		IF (("&HLOS_TYPE"=="LA")||("&HLOS_TYPE"=="la"))
		(
			DIALOG
			(&
				HEADER "Build Options"
				
				POS 0. 0. 75. 35.
				BOX "Select Image Sources"
				POS 1. 1. 70. 1.
				BOOTTEXT: TEXT "Boot Build"
				BOOTEDIT: EDIT "&BOOT_BUILDROOT" "GOSUB VERIFYBOOTBUILD"

				RPMTEXT: TEXT "RPM Build"
				RPMEDIT: DEFEDIT "&RPM_BUILDROOT" "GOSUB VERIFYRPMBUILD"

				TZTEXT: TEXT "TZ Build"
				TZEDIT: DEFEDIT "&TZ_BUILDROOT" "GOSUB VERIFYTZBUILD"

				APPSTEXT: TEXT "APPS Build"
				APPSEDIT: DEFEDIT "&APPS_BUILDROOT" "GOSUB VERIFYAPPSBUILD"

				MPSSTEXT: TEXT "MPSS Build"
				MPSSEDIT: DEFEDIT "&MPSS_BUILDROOT" "GOSUB VERIFYMPSSBUILD"

				
				WCNSSTEXT: TEXT "WCNSS Build"
				WCNSSEDIT: DEFEDIT "&WCNSS_BUILDROOT" "GOSUB VERIFYWCNSSBUILD"
				
				ADSPTEXT: TEXT "ADSP Build"
				ADSPEDIT: DEFEDIT "&ADSP_BUILDROOT" "GOSUB VERIFYADSPBUILD"

				CPETEXT: TEXT "CPE Build"
				CPEEDIT: DEFEDIT "&CPE_BUILDROOT" "GOSUB VERIFYCPEBUILD"

				VIDEOTEXT: TEXT "VIDEO Build"
				VIDEOEDIT: DEFEDIT "&VENUS_BUILDROOT" "GOSUB VERIFYVIDEOBUILD"

				POS 1. 21. 6. 1.
				HELPBUTTON: DEFBUTTON "?"
				(
					DIALOG.OK "Paste the paths of the various builds for this debug session. Click the map button after that. The defaults are provided by the meta build."
				)
				POS 48. 21. 6. 1.
				MAPBUTTON: DEFBUTTON "Map" "GOSUB BUILDSYNCUP"

				POS 1. 23. 37. 11.
				BOX "Load Options"
				POS 2. 24. 34. 1.
				LOAD.ERASEONLY: CHOOSEBOX "Erase Storage only" "GOSUB SETLOADOPTION"
				LOAD.COMMON: CHOOSEBOX "Load Common Images Only (BOOT, TZ,HYP, RPM)" "GOSUB SETLOADOPTION"
				LOAD.FULL: CHOOSEBOX "Load Full Build" "GOSUB SETLOADOPTION"
				LOAD.FULL.SECONDARY: CHOOSEBOX "Load Full Build with Secondary Boot and System" "GOSUB SETLOADOPTION"
				LOAD.SINGLEIMG: CHOOSEBOX "Load single image" "GOSUB SETLOADOPTION"
				LOADIMGSELECT: DYNPULLDOWN "sbl,tz,rpm,appsboot" "GOSUB SETLOADOPTION"


				POS 2. 30. 14. 1.
				LOADMETHODTEXT: TEXT "Loading Method"
				POS 3. 31. 14. 1.
				LOADMETHOD.JTAG: CHOOSEBOX "JTag" "GOSUB SET_LOADMETHOD"
				LOADMETHOD.DEVPROGRAMMER: CHOOSEBOX "Device Programmer" "GOSUB SET_LOADMETHOD"
        
				POS 29. 31. 8. 1. 
				HELPBUTTON: DEFBUTTON "?"
				(
					DIALOG.OK "Common Images option will load the bootloaders, RPM and TZ images. Full Build option will load all images including HLOS images. Single image option lets you choose single binary to load. Device programmer will load the full build via Sahara and FireHose protocol over USB"
				)

				POS 48. 27. 6. 1.
				LOADBUTTON: DEFBUTTON "Load" "GOSUB LOADBUILD"

				POS 45. 23. 25. 3.
				BOX "Product Flavor"
				POS 46. 24. 23. 1.
				PDFLAVOR: DYNPULLDOWN "&PRODUCT_FLAVORS" "GOSUB LOADPDFLAVORS"
				
				//POS 56. 22. 5. 1.
				//LOADBUTTON: DEFBUTTON "Help" "GOSUB HELP"
			)

		)	
		IF (("&HLOS_TYPE"=="wp")||("&HLOS_TYPE"=="WP"))
			(
				DIALOG
				(&
					HEADER "Build Options"
					
					POS 0. 0. 71. 31.
					BOX "Select Image Sources"
					POS 1. 1. 70. 1.
					BOOTTEXT: TEXT "Boot Build"
					BOOTEDIT: EDIT "&BOOT_BUILDROOT" "GOSUB VERIFYBOOTBUILD"

					RPMTEXT: TEXT "RPM Build"
					RPMEDIT: DEFEDIT "&RPM_BUILDROOT" "GOSUB VERIFYRPMBUILD"

					TZTEXT: TEXT "TZ Build"
					TZEDIT: DEFEDIT "&TZ_BUILDROOT" "GOSUB VERIFYTZBUILD"

					APPSTEXT: TEXT "APPS Build"
					APPSEDIT: DEFEDIT "&APPS_BUILDROOT" "GOSUB VERIFYAPPSBUILD"

					MPSSTEXT: TEXT "MPSS Build"
					MPSSEDIT: DEFEDIT "&MPSS_BUILDROOT" "GOSUB VERIFYMPSSBUILD"

					
					WCNSSTEXT: TEXT "WCNSS Build"
					WCNSSEDIT: DEFEDIT "&WCNSS_BUILDROOT" "GOSUB VERIFYWCNSSBUILD"
					
					ADSPTEXT: TEXT "ADSP Build"
					ADSPEDIT: DEFEDIT "&ADSP_BUILDROOT" "GOSUB VERIFYADSPBUILD"	
					
					WINSECAPPTEXT: TEXT "WINSECAPP Build"
					WINSECAPPEDIT: DEFEDIT "&WINSECAPP_BUILDROOT" "GOSUB VERIFYWINSECAPPBUILD"	

					POS 1. 18. 6. 1. 
					HELPBUTTON: DEFBUTTON "?"
					(
						DIALOG.OK "Paste the paths of the various builds for this debug session. Click the map button after that. The defaults are provided by the meta build."
					)
					POS 48. 18. 6. 1.
					MAPBUTTON: DEFBUTTON "Map" "GOSUB BUILDSYNCUP"

					POS 1. 20. 37. 10.
					BOX "Load Options"
					POS 2. 21. 34. 1.
					LOAD.ERASEONLY: CHOOSEBOX "Erase Storage only" "GOSUB SETLOADOPTION"
					LOAD.COMMON: CHOOSEBOX "Load Common Images Only (BOOT, TZ, RPM)" "GOSUB SETLOADOPTION"
					LOAD.FULL: CHOOSEBOX "Load Full Build" "GOSUB SETLOADOPTION"
					LOAD.SINGLEIMG: CHOOSEBOX "Load single image" "GOSUB SETLOADOPTION"
					LOADIMGSELECT: DYNPULLDOWN "sbl,tz,rpm,appsboot" "GOSUB SETLOADOPTION"
					
					POS 2. 26. 14. 1.
					LOADMETHODTEXT: TEXT "Loading Method"
					POS 3. 27. 14. 1.
					LOADMETHOD.JTAG: CHOOSEBOX "JTag" "GOSUB SET_LOADMETHOD"
					LOADMETHOD.DEVPROGRAMMER: CHOOSEBOX "Device Programmer" "GOSUB SET_LOADMETHOD"

					POS 28. 28. 6. 1.
					HELPBUTTON: DEFBUTTON "?"
					(
						DIALOG.OK "Common Images option will load the bootloaders, RPM and TZ images. Full Build option will load all images including HLOS images."
					)

					POS 48. 22. 6. 1.
					LOADBUTTON: DEFBUTTON "Load" "GOSUB LOADBUILD"

					POS 45. 19. 25. 3.
					BOX "Product Flavor"
					POS 46. 20. 23. 1.
					PDFLAVOR: DYNPULLDOWN "&PRODUCT_FLAVORS" "GOSUB LOADPDFLAVORS"
					
					//POS 47. 20. 24. 2.
					//LOADBUTTON: DEFBUTTON "Help" "GOSUB HELP"

				)
			)	
		
	    IF (("&HLOS_TYPE"=="le")||("&HLOS_TYPE"=="LE"))
		(
			DIALOG
			(&
				HEADER "Build Options"
				
				POS 0. 0. 75. 35.
				BOX "Select Image Sources"
				POS 1. 1. 70. 1.
				BOOTTEXT: TEXT "Boot Build"
				BOOTEDIT: EDIT "&BOOT_BUILDROOT" "GOSUB VERIFYBOOTBUILD"

				RPMTEXT: TEXT "RPM Build"
				RPMEDIT: DEFEDIT "&RPM_BUILDROOT" "GOSUB VERIFYRPMBUILD"

				TZTEXT: TEXT "TZ Build"
				TZEDIT: DEFEDIT "&TZ_BUILDROOT" "GOSUB VERIFYTZBUILD"

				APPSTEXT: TEXT "APPS Build"
				APPSEDIT: DEFEDIT "&APPS_BUILDROOT" "GOSUB VERIFYAPPSBUILD"

				MPSSTEXT: TEXT "MPSS Build"
				MPSSEDIT: DEFEDIT "&MPSS_BUILDROOT" "GOSUB VERIFYMPSSBUILD"

				
				WCNSSTEXT: TEXT "WCNSS Build"
				WCNSSEDIT: DEFEDIT "&WCNSS_BUILDROOT" "GOSUB VERIFYWCNSSBUILD"
				
				ADSPTEXT: TEXT "ADSP Build"
				ADSPEDIT: DEFEDIT "&ADSP_BUILDROOT" "GOSUB VERIFYADSPBUILD"	

				CPETEXT: TEXT "CPE Build"
				CPEEDIT: DEFEDIT "&CPE_BUILDROOT" "GOSUB VERIFYCPEBUILD"

				

				POS 1. 21. 6. 1.
				HELPBUTTON: DEFBUTTON "?"
				(
					DIALOG.OK "Paste the paths of the various builds for this debug session. Click the map button after that. The defaults are provided by the meta build."
				)
				POS 48. 21. 6. 1.
				MAPBUTTON: DEFBUTTON "Map" "GOSUB BUILDSYNCUP"

				POS 1. 23. 37. 11.
				BOX "Load Options"
				POS 2. 24. 34. 1.
				LOAD.ERASEONLY: CHOOSEBOX "Erase Storage only" "GOSUB SETLOADOPTION"
				LOAD.COMMON: CHOOSEBOX "Load Common Images Only (BOOT, TZ,HYP, RPM)" "GOSUB SETLOADOPTION"
				LOAD.FULL: CHOOSEBOX "Load Full Build" "GOSUB SETLOADOPTION"
				LOAD.FULL.SECONDARY: CHOOSEBOX "Load Full Build with Secondary Boot and System" "GOSUB SETLOADOPTION"
				LOAD.SINGLEIMG: CHOOSEBOX "Load single image" "GOSUB SETLOADOPTION"
				LOADIMGSELECT: DYNPULLDOWN "sbl,tz,rpm,appsboot" "GOSUB SETLOADOPTION"


				POS 2. 30. 14. 1.
				LOADMETHODTEXT: TEXT "Loading Method"
				POS 3. 31. 14. 1.
				LOADMETHOD.JTAG: CHOOSEBOX "JTag" "GOSUB SET_LOADMETHOD"
				LOADMETHOD.DEVPROGRAMMER: CHOOSEBOX "Device Programmer" "GOSUB SET_LOADMETHOD"
        
				POS 29. 31. 8. 1. 
				HELPBUTTON: DEFBUTTON "?"
				(
					DIALOG.OK "Common Images option will load the bootloaders, RPM and TZ images. Full Build option will load all images including HLOS images. Single image option lets you choose single binary to load. Device programmer will load the full build via Sahara and FireHose protocol over USB"
				)

				POS 48. 27. 6. 1.
				LOADBUTTON: DEFBUTTON "Load" "GOSUB LOADBUILD"

				POS 45. 23. 25. 3.
				BOX "Product Flavor"
				POS 46. 24. 23. 1.
				PDFLAVOR: DYNPULLDOWN "&PRODUCT_FLAVORS" "GOSUB LOADPDFLAVORS"
				
			//	POS 56. 22. 5. 1.
			//	LOADBUTTON: DEFBUTTON "Help" "GOSUB HELP"
			)

		)	
	
	// Set the default options here
    GOSUB CHECKLOAD
    DIALOG.SET LOAD.FULL
    &LOAD_OPT="LOADFULL"
    IF FILE.EXIST("&METASCRIPTSDIR/../deviceprogrammer/deviceprogrammer.cmm")
    (
        //keep this on JTAG for now. 
        //Later will make default device programmer
        DIALOG.SET LOADMETHOD.JTAG
        &LOAD_METHOD="JTAG"
    )
    //If Deviceprogrammer has been disabled
    ELSE
    (
        DIALOG.DISABLE LOADMETHOD.DEVPROGRAMMER
        DIALOG.SET LOADMETHOD.JTAG
        &LOAD_METHOD="JTAG"
    )
    
    RETURN



SET_LOADMETHOD:
    IF DIALOG.BOOLEAN(LOADMETHOD.DEVPROGRAMMER)
    (
        &LOAD_METHOD="DEVPROG"
    )
    ELSE
    (
        &LOAD_METHOD="JTAG"
        DIALOG.SET LOADMETHOD.JTAG
    )	



SETLOADOPTION:
	IF DIALOG.BOOLEAN(LOAD.ERASEONLY)
	(
		&LOAD_OPT="ERASEONLY"
	)
	IF DIALOG.BOOLEAN(LOAD.COMMON)
	(
		&LOAD_OPT="LOADCOMMON"
	)
	IF DIALOG.BOOLEAN(LOAD.FULL)
	(
		&LOAD_OPT="LOADFULL"
        IF FILE.EXIST("&METASCRIPTSDIR/../deviceprogrammer/deviceprogrammer.cmm")
        (
            DIALOG.ENABLE LOADMETHOD.DEVPROGRAMMER
        )
	)
	IF DIALOG.BOOLEAN(LOAD.FULL.SECONDARY)
	(
		&LOAD_OPT="LOADFULLSECONDARY"
		IF FILE.EXIST("&METASCRIPTSDIR/../deviceprogrammer/deviceprogrammer.cmm")
        (
            DIALOG.ENABLE LOADMETHOD.DEVPROGRAMMER
        )
	)
	IF DIALOG.BOOLEAN(LOAD.SINGLEIMG)
    (
		&LOAD_OPT="LOADIMG"
        &LOAD_IMG=DIALOG.STRING(LOADIMGSELECT)
        IF ("&LOAD_IMG"=="")
        (
            DIALOG.DISABLE LOADBUTTON
        )
        ELSE
        (
            DIALOG.ENABLE LOADBUTTON
        )
	    DIALOG.DISABLE LOADMETHOD.DEVPROGRAMMER
        DIALOG.SET LOADMETHOD.JTAG
	)
	
	RETURN

// Macros to check for valid build locations
// Expand to include more comprehensive checks
VERIFYBOOTBUILD:
	LOCAL &DIR

	&DIR=DIALOG.STRING(BOOTEDIT)
	IF !OS.DIR("&DIR/boot_images")
	(
		DIALOG.SET BOOTEDIT "Invalid Boot Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&BOOT_BUILDROOT="&DIR"
		DIALOG.SET BOOTEDIT "&BOOT_BUILDROOT"
		// We have a new build
		&NEW_BOOTBUILD=1.
		GOSUB CHECKLOAD
	)
	RETURN

VERIFYTZBUILD:
	LOCAL &DIR

	&DIR=DIALOG.STRING(TZEDIT)
	IF !OS.DIR("&DIR/trustzone_images")
	(
		DIALOG.SET TZEDIT "Invalid TZ Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&TZ_BUILDROOT="&DIR"
		DIALOG.SET TZEDIT "&TZ_BUILDROOT"
		&NEW_TZBUILD=1.
		GOSUB CHECKLOAD
	)
	RETURN

VERIFYRPMBUILD:
	LOCAL &DIR

	&DIR=DIALOG.STRING(RPMEDIT)
	IF !OS.DIR("&DIR/rpm_proc")
	(
		DIALOG.SET RPMEDIT "Invalid RPM Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&RPM_BUILDROOT="&DIR"
		DIALOG.SET RPMEDIT "&RPM_BUILDROOT"
		&NEW_RPMBUILD=1.
		GOSUB CHECKLOAD
	)
	RETURN

VERIFYAPPSBUILD:
	LOCAL &DIR
	
	&DIR=DIALOG.STRING(APPSEDIT)
	&APPS_BUILDROOT="&DIR"
	&NEW_APPSBUILD=1.
	GOSUB CHECKLOAD
	
	RETURN

VERIFYMPSSBUILD:
	LOCAL &DIR

	&DIR=DIALOG.STRING(MPSSEDIT)
	IF !OS.DIR("&DIR/modem_proc")
	(
		DIALOG.SET MPSSEDIT "Invalid MPSS Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&MPSS_BUILDROOT="&DIR"
		DIALOG.SET MPSSEDIT "&MPSS_BUILDROOT"
		&NEW_MPSSBUILD=1.
		GOSUB CHECKLOAD
	)
	RETURN


	
VERIFYWCNSSBUILD:
	LOCAL &DIR

	&DIR=DIALOG.STRING(WCNSSEDIT)
	IF !OS.DIR("&DIR/wcnss_proc")
	(
		DIALOG.SET WCNSSEDIT "Invalid WCNSS Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.ENABLE MAPBUTTON
	)
	ELSE
	(
		&WCNSS_BUILDROOT="&DIR"
		DIALOG.SET WCNSSEDIT "&WCNSS_BUILDROOT"
		&NEW_WCNSSBUILD=1.
		GOSUB CHECKLOAD
	)
	RETURN
		
VERIFYADSPBUILD:
	LOCAL &DIR

	&DIR=DIALOG.STRING(ADSPEDIT)
	IF !OS.DIR("&DIR/adsp_proc")
	(
		DIALOG.SET ADSPEDIT "Invalid ADSP Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&ADSP_BUILDROOT="&DIR"
		DIALOG.SET ADSPEDIT "&ADSP_BUILDROOT"
		&NEW_ADSPBUILD=1.
		GOSUB CHECKLOAD
	)
	RETURN
VERIFYWINSECAPPBUILD:
	LOCAL &DIR

	&DIR=DIALOG.STRING(WINSECAPPEDIT)
	IF !OS.DIR("&DIR/winsecapp_image")
	(
		DIALOG.SET WINSECAPPEDIT "Invalid WINSECAPP Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&WINSECAPP_BUILDROOT="&DIR"
		DIALOG.SET WINSECAPPEDIT "&WINSECAPP_BUILDROOT"
		&NEW_WINSECAPPBUILD=1.
		GOSUB CHECKLOAD
	)
	RETURN
VERIFYCPEBUILD:
	LOCAL &DIR
	
	&DIR=DIALOG.STRING(CPEEDIT)
	IF !OS.DIR("&DIR/cpe_proc")
	(
		DIALOG.SET CPEEDIT "Invalid CPE Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&CPE_BUILDROOT="&DIR"
		DIALOG.SET CPEEDIT "&CPE_BUILDROOT"
		&NEW_CPEBUILD=1
		GOSUB CHECKLOAD
	)
	RETURN
VERIFYVIDEOBUILD:
	LOCAL &DIR
	
	&DIR=DIALOG.STRING(VIDEOEDIT)
	IF !OS.DIR("&DIR/venus_proc")
	(
		DIALOG.SET VIDEOEDIT "Invalid VIDEO Build"
		DIALOG.DISABLE LOADBUTTON
		DIALOG.DISABLE MAPBUTTON
	)
	ELSE
	(
		&VENUS_BUILDROOT="&DIR"
		DIALOG.SET VIDEOEDIT "&VENUS_BUILDROOT"
		&NEW_VIDEOBUILD=1
		GOSUB CHECKLOAD
	)
	RETURN	
CHECKLOAD:
	LOCAL &DIR
	
		IF (OS.DIR("&RPM_BUILDROOT/rpm_proc")&&OS.DIR("&BOOT_BUILDROOT/boot_images")&&OS.DIR("&MPSS_BUILDROOT/modem_proc")&&OS.DIR("&WCNSS_BUILDROOT/wcnss_proc")&&OS.DIR("&ADSP_BUILDROOT/adsp_proc")&&OS.DIR("&CPE_BUILDROOT/cpe_proc")&&OS.DIR("&VENUS_BUILDROOT/venus_proc"))
		(
			IF (("&HLOS_TYPE"=="LA")||("&HLOS_TYPE"=="la")||("&HLOS_TYPE"=="LE")||("&HLOS_TYPE"=="le"))
			(
				DIALOG.ENABLE LOADBUTTON
				DIALOG.ENABLE MAPBUTTON
			)
			ELSE IF (("&HLOS_TYPE"=="WP")||("&HLOS_TYPE"=="wp"))
			(
				IF (OS.DIR("&WINSECAPP_BUILDROOT/winsecapp_image"))
				(
					DIALOG.ENABLE LOADBUTTON
					DIALOG.ENABLE MAPBUTTON
				)
				ELSE
				(
					DIALOG.DISABLE LOADBUTTON
					DIALOG.ENABLE MAPBUTTON
				)
			)
		)
	
		ELSE
		(
		DIALOG.DISABLE LOADBUTTON
		DIALOG.ENABLE MAPBUTTON
		)
	
	RETURN


LOADPDFLAVORS:
	// Basically call the script generated by the meta-build with the image information
	// using the user specified product flavor
	&FLAVOR_OPT=DIALOG.STRING(PDFLAVOR)
	
	do gen_buildflavor &FLAVOR_OPT
	// Change the global that indicates the product flavor chosen
	&PRODUCT_FLAVOR="&FLAVOR_OPT"
	&NEW_PDFLAVOR=1.

	RETURN


BUILDSYNCUP:
	// Assumptions: 
	// 1. &NEW_XYZBUILD variable is populated correctly to indicate the state 
	// of the build. A non-zero value indicates that user has changed the build from what 
	// was present in the meta build.
	// 2. Utility is always run from the APPS T32 session
	// 3. Every processor cares only about its own image. Apps is the only exception (at times).
	// So we notify only necessary procs that their build has changed. 
	
	IF ("&NEW_RPMBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds RPM &RPM_BUILDROOT
		do std_intercom_do &RPM_PORT std_mapbuilds RPM &RPM_BUILDROOT
		// Reload the session configs again 
		do std_intercom_do &RPM_PORT std_sessioncfg_rpm
	)
	IF ("&NEW_BOOTBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds BOOT &BOOT_BUILDROOT
		
	)
	IF ("&NEW_TZBUILD">"0.")
	(
        do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds TZ &TZ_BUILDROOT
	)
	IF ("&NEW_APPSBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds APPS &APPS_BUILDROOT
			// Reload the session configs again 
		do std_intercom_do &CLUSTER1_APPS0_PORT std_sessioncfg_apps
	)
	IF ("&NEW_MPSSBUILD">"0.")
	(	
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds MPSS &MPSS_BUILDROOT
		do std_intercom_do &MPSS_SCALAR_PORT  std_mapbuilds MPSS &MPSS_BUILDROOT
		// Reload the session configs again 
		do std_intercom_do &MPSS_SCALAR_PORT std_sessioncfg_mpss
	)
	
	IF ("&NEW_WCNSSBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds WCNSS &WCNSS_BUILDROOT
		do std_intercom_do &WCNSS_PORT std_mapbuilds WCNSS &WCNSS_BUILDROOT
		// Reload the session configs again 
		do std_intercom_do &WCNSS_PORT std_sessioncfg_wcnss
	)
	
	IF ("&NEW_PDFLAVOR">"0.")
	(
	 	// New product flavor has been selected and has to be broadcast to all
		// sessions
	
			do std_intercom_do &CLUSTER1_APPS0_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &CLUSTER1_APPS1_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &CLUSTER1_APPS2_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &CLUSTER1_APPS3_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &CLUSTER0_APPS0_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &CLUSTER0_APPS1_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &CLUSTER0_APPS2_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &CLUSTER0_APPS3_PORT gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &MPSS_SCALAR_PORT  gen_buildflavor &FLAVOR_OPT
			do std_intercom_do &WCNSS_PORT gen_buildflavor &FLAVOR_OPT	
	)
    
	IF ("&NEW_ADSPBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds ADSP &ADSP_BUILDROOT
		do std_intercom_do &ADSP_PORT  std_mapbuilds ADSP &ADSP_BUILDROOT
		// Reload the session configs again 
		do std_intercom_do &ADSP_PORT std_sessioncfg_adsp
	)
	IF ("&NEW_WINSECAPPBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds WINSECAPP &WINSECAPP_BUILDROOT
	)
	IF ("&NEW_CPEBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds CPE &CPE_BUILDROOT
	)
	IF ("&NEW_VIDEOBUILD">"0.")
	(
		do std_intercom_do &CLUSTER1_APPS0_PORT std_mapbuilds VIDEO &VENUS_BUILDROOT
	)
	
	RETURN
//////////////////////////////////////////
//
//          HELP
//          public function
//          Prints usage information
//          Expected input: None
//
/////////////////////////////////////////
HELP:
    AREA.RESET
    AREA.CREATE std_loadbuild_help 160. 53.
    AREA.SELECT std_loadbuild_help
    WINPOS 0. 0. 160. 53.
    AREA.VIEW std_loadbuild_help

    PRINT " "
    PRINT "  /////////////////std_loadbuild HELP/////////////////////"
    PRINT " "
    PRINT "  Top level JTag Build Loader"
    PRINT " "
    PRINT "  Std_loadbuild wraps several scripts which program the target with the specified images."
    PRINT "     Note that std_loadbuild will only flash those images necessary ('common images') to get the  "
    PRINT "     target to apps bootloader, at which time it allows the system to boot up and passes control "
    PRINT "     to another tool to take over loading from there (for Android - fastboot; for WP - FFU tool)."
    PRINT "  "
    PRINT "  IMPORTANT: Note that because of this, updating the paths for non-common images (such as peripheral cores) <<<will not>>> result in those "
    PRINT "             peripheral cores' software being updated, since std_loadbuild doesn't have control of software at those points"
    PRINT "             Only updating paths to common images such as RPM, TZ, XBL etc. will have effect"
    PRINT " "
    PRINT "  Common Issues"
    PRINT "     Reset - std_loadbuild first resets the target. If the target is in some sleep state or some boot point prior to reset, "
    PRINT "             issues can occur. It is recommended to reset the target and run these scripts a few seconds after device starts booting up"
    PRINT "     Path issues -   std_loadbuild calls jtagprogrammer.cmm scripts iteratavely on each partition. "
    PRINT "                     If this script is not available, std_loadbuild won't work. Also, if there"
    PRINT "                     are issues with this script, you may see hangs. Contact Qualcomm storage team if "
    PRINT "                     You are observing hangs during JTag load."
    PRINT "     XML Location - XML files are needed for jtagprogrammer. These should be located in <Metabuild Root>/common/build/<storage_type>,"
    PRINT "                    Where <storage_type> is either ufs or emmc"
    PRINT "     Error messages in jtagprogrammer.cmm - if you see error messages when jtagprogrammer is running, such as  "
    PRINT "                                            'ERROR: sbl.elf not found in rawprograme3.xml', These are expected during the loading process and are an "
    PRINT "                                            artifact from the fashion that std_loadbuild passes commands to the jtag programmer script. "
    PRINT "                   "
    PRINT " "

    PRINT "  Product Flavor - "
    PRINT "         Different product flavors provided by target team. "
    PRINT "         Not necessary to set this for full build load"
    PRINT "  Load Options - "
    PRINT "         Erase Storage Only - erases storage (e.g. erases UFS or EMMC card)"
    PRINT "         Load common images only  - Goes through load process but doesn't call HLOS loader at the end of execution"
    PRINT "         Load Full Build - Loads the common images then passes control to HLOS loader"
    PRINT "         Load Single Image - Allows user to only load images specific to images within common images "
    PRINT "                         e.g. all TZ-related images, or RPM image, or SBL/PMIC images etc."
    PRINT "         Loading method"
    PRINT "             JTag - traditional JTag method. This calls Msjload.cmm scripts from sbl image"
    PRINT "             Device Programmer - Uses hybrid Jtag and Sahara/Firehose download via USB. Internal-Qualcomm use only"
    PRINT "  Load - "
    PRINT "         Executes command specified in 'Load Options'. (E.G. load full build, load single  image, or load  common images)"
    PRINT "         and from different cores (ADSP, MPSS, SLPI, RPM etc.) to a state that the user can debug a crash on that core"
    PRINT "         There are several options and configurations, listed below in the Command line usage area"
    PRINT "  "
    PRINT " "
    PRINT " "
	
	
	RETURN

EXIT:
	ENDDO
