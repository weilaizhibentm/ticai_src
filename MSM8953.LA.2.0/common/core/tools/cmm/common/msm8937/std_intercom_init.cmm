//============================================================================
//  Name:                                                                     
//    std_intercom_init.cmm
//
//  Description:                                                              
//    It initializes the intercom ports for all the subsystems. It will ping
//    a particular sub-system to check if it is "alive" depending on the arguments
//    passes
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who     		what, where, why
// --------   ---     		---------------------------------------------------------
// 07/19/2012 AJCheriyan	Fixed localhost issue and renamed intercom ports 
// 07/10/2012 AJCheriyan    Created for B-family
//
//

// We support two values for the first argument with arguments = no.of sub-systems + 1  
// IARG0 - NEWSESSION or CHECKSESSION. If NEWSESSION, we just initialize the intercom ports and return
// 		  If CHECKSESSION is chosen, sessions indicated by ARG1 - ARGn are checked and global
// 		  variables are updated
// IARG1 - No. of sessions to check. Used only if ARG0 is CHECKSESSION
// IARGn - n > 1 - SESSION names to check for. Currently supports the following
// 		  Cluster 1 APPS0, Cluster 1 APPS1, Cluster 1 APPS2, Cluster 1 APPS3, 
//		  Cluster 0 APPS0, Cluster 0 APPS1, Cluster 0 APPS2, Cluster 0 APPS3, RPM, MPSS, WCNSS

ENTRY &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11 &IARG12

// Globals: Global Variables
// Global variables / intercom handles for every subsystem
GLOBAL &CLUSTER1_APPS0_ALIVE &CLUSTER1_APPS0_PORT 
GLOBAL &CLUSTER1_APPS1_ALIVE &CLUSTER1_APPS1_PORT 
GLOBAL &CLUSTER1_APPS2_ALIVE &CLUSTER1_APPS2_PORT 
GLOBAL &CLUSTER1_APPS3_ALIVE &CLUSTER1_APPS3_PORT
GLOBAL &CLUSTER0_APPS0_ALIVE &CLUSTER0_APPS0_PORT 
GLOBAL &CLUSTER0_APPS1_ALIVE &CLUSTER0_APPS1_PORT 
GLOBAL &CLUSTER0_APPS2_ALIVE &CLUSTER0_APPS2_PORT 
GLOBAL &CLUSTER0_APPS3_ALIVE &CLUSTER0_APPS3_PORT
GLOBAL &RPM_ALIVE &RPM_PORT
GLOBAL &MPSS_SCALAR_ALIVE &MPSS_SCALAR_PORT
GLOBAL &MPSS_VECTOR_ALIVE &MPSS_VECTOR_PORT
GLOBAL &ADSP_ALIVE &ADSP_PORT
GLOBAL &WCNSS_ALIVE &WCNSS_PORT

//Boot processor port
GLOBAL &APPS0_PORT

// Handle for this processor
GLOBAL &LOCALHOST
LOCAL &RVAL
MAIN:
	 &RVAL=0

   // Setup intercom ports only if NEWSESSION	
   IF ("&IARG0"=="NEWSESSION")
   (

	// Intercom port number of the currrent session
   	&LOCALHOST=INTERCOM.PORT()

	&CLUSTER1_APPS0_PORT="10000."
	&CLUSTER1_APPS1_PORT="10001."
	&CLUSTER1_APPS2_PORT="10002."
	&CLUSTER1_APPS3_PORT="10003."
	&CLUSTER0_APPS0_PORT="10004."
	&CLUSTER0_APPS1_PORT="10005."
	&CLUSTER0_APPS2_PORT="10006."
	&CLUSTER0_APPS3_PORT="10007."
	&RPM_PORT="10008."
	&MPSS_SCALAR_PORT="10009."
	&MPSS_VECTOR_PORT="10010."
	&ADSP_PORT="10011."
	&WCNSS_PORT="10012."

	//8952 cluster1 apps0 is boot core.
	&APPS0_PORT=&CLUSTER1_APPS0_PORT
  )

  // We want to check if a particular session is 
  // active. So ping and find out 
  IF ("&IARG0"=="CHECKSESSION")
  (
	GOSUB CHECKSESSIONARG &IARG2
	GOSUB CHECKSESSIONARG &IARG3
	GOSUB CHECKSESSIONARG &IARG4
	GOSUB CHECKSESSIONARG &IARG5
	GOSUB CHECKSESSIONARG &IARG6
	GOSUB CHECKSESSIONARG &IARG7
	GOSUB CHECKSESSIONARG &IARG8
	GOSUB CHECKSESSIONARG &IARG9
	GOSUB CHECKSESSIONARG &IARG10
	GOSUB CHECKSESSIONARG &IARG11
	GOSUB CHECKSESSIONARG &IARG12
  )

  GOTO EXIT


 // Function to check if the session is valid or not
 // If invalid, it will exit the script. The assumption
 // is that the first argument which is "NULL" will be 
 // followed by more NULL arguments and hence, can exit. 
 // The arguments must be checked in serial order for this 
 // to be true.
CHECKSESSIONARG:
	ENTRY &SESSION

	IF ("&SESSION"=="")
	(
		GOTO EXIT
	)

	IF ("&SESSION"=="CLUSTER1_APPS0")
	(
	 	IF INTERCOM.PING(&CLUSTER1_APPS0_PORT)
	  	(
	 		&CLUSTER1_APPS0_ALIVE=1.
			 &RVAL=1.
	   		PRINT "Cluster 1 Apps Session 0 Active."
		)
		ELSE
		(
 			&CLUSTER1_APPS0_ALIVE=0.
   		)
		
	)

	IF ("&SESSION"=="CLUSTER1_APPS1")
	(
	 	IF INTERCOM.PING(&CLUSTER1_APPS1_PORT)
	  	(
	 		&CLUSTER1_APPS1_ALIVE=1.
			&RVAL=1.
	   		PRINT "Cluster 1 Apps Session 1 Active."
		)
		ELSE
		(
 			&CLUSTER1_APPS0_ALIVE=0.
   		)
		
	)

	IF ("&SESSION"=="CLUSTER1_APPS2")
	(
	 	IF INTERCOM.PING(&CLUSTER1_APPS2_PORT)
	  	(
	 		&CLUSTER1_APPS2_ALIVE=1.
			&RVAL=1.
	   		PRINT "Cluster 1 Apps Session 2 Active."
		)
		ELSE
		(
 			&CLUSTER1_APPS2_ALIVE=0.
   		)
		
	)
	
	IF ("&SESSION"=="CLUSTER1_APPS3")
	(
	 	IF INTERCOM.PING(&CLUSTER1_APPS3_PORT)
	  	(
	 		&CLUSTER1_APPS3_ALIVE=1.
			&RVAL=1.
	   		PRINT "Cluster 1 Apps Session 3 Active."
		)
		ELSE
		(
 			&CLUSTER1_APPS3_ALIVE=0.
   		)
		
	)
	
	IF ("&SESSION"=="RPM")
	(
	 	IF INTERCOM.PING(&RPM_PORT)
	  	(
	 		&RPM_ALIVE=1.
			&RVAL=1.
	   		PRINT "RPM Session Active."
		)
		ELSE
		(
 			&RPM_ALIVE=0.
   		)
		
	)
	
	IF ("&SESSION"=="CLUSTER0_APPS0")
	(
	 	IF INTERCOM.PING(&CLUSTER0_APPS0_PORT)
	  	(
	 		&CLUSTER0_APPS0_ALIVE=1.
			&RVAL=1.
	   		PRINT "Cluster 0 Apps Session 0 Active."
		)
		ELSE
		(
 			&CLUSTER0_APPS0_ALIVE=0.
   		)
		
	)

	IF ("&SESSION"=="CLUSTER0_APPS1")
	(
	 	IF INTERCOM.PING(&CLUSTER0_APPS1_PORT)
	  	(
	 		&CLUSTER0_APPS1_ALIVE=1.
			&RVAL=1.
	   		PRINT "Cluster 0 Apps Session 1 Active."
		)
		ELSE
		(
 			&CLUSTER0_APPS0_ALIVE=0.
   		)
		
	)

	IF ("&SESSION"=="CLUSTER0_APPS2")
	(
	 	IF INTERCOM.PING(&CLUSTER0_APPS2_PORT)
	  	(
	 		&CLUSTER0_APPS2_ALIVE=1.
			&RVAL=1.
	   		PRINT "Cluster 0 Apps Session 2 Active."
		)
		ELSE
		(
 			&CLUSTER0_APPS2_ALIVE=0.
   		)
		
	)
	
	IF ("&SESSION"=="CLUSTER0_APPS3")
	(
	 	IF INTERCOM.PING(&CLUSTER0_APPS3_PORT)
	  	(
	 		&CLUSTER0_APPS3_ALIVE=1.
			&RVAL=1.
	   		PRINT "Cluster 0 Apps Session 3 Active."
		)
		ELSE
		(
 			&CLUSTER0_APPS3_ALIVE=0.
   		)
		
	)
	
	IF ("&SESSION"=="MPSS_SCALAR")
	(
	 	IF INTERCOM.PING(&MPSS_SCALAR_PORT)
	  	(
	 		&MPSS_SCALAR_ALIVE=1.
			&RVAL=1.
	   		PRINT "MPSS Scalar Session Active."
		)
		ELSE
		(
 			&MPSS_SCALAR_ALIVE=0.
   		)
		
	)

	IF ("&SESSION"=="MPSS_VECTOR")
	(
	 	IF INTERCOM.PING(&MPSS_VECTOR_PORT)
	  	(
	 		&MPSS_VECTOR_ALIVE=1.
			&RVAL=1.
	   		PRINT "MPSS Vector Session Active."
		)
		ELSE
		(
 			&MPSS_VECTOR_ALIVE=0.
   		)
		
	)
	
	IF ("&SESSION"=="ADSP")
	(
	 	IF INTERCOM.PING(&ADSP_PORT)
	  	(
	 		&ADSP_ALIVE=1.
			&RVAL=1.
	   		PRINT "ADSP Session Active."
		)
		ELSE
		(
 			&ADSP_ALIVE=0.
   		)
		
	)

	IF ("&SESSION"=="WCNSS")
	(
	 	IF INTERCOM.PING(&WCNSS_PORT)
	  	(
	 		&WCNSS_ALIVE=1.
			&RVAL=1.
	   		PRINT "WCNSS Session Active."
		)
		ELSE
		(
 			&WCNSS_ALIVE=0.
   		)
		
	)

	RETURN &RVAL
	
EXIT:
	ENDDO &RVAL
