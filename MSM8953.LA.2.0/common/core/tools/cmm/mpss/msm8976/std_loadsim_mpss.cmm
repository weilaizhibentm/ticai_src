//============================================================================
//  Name:                                                                     
//    std_loadsim_mpss.cmm 
//
//  Description:                                                              
//    Script to load MPSS logs
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//  Description:
//  T32 simulator loader specific to modem dumps. This file has some 8996 specific areas.
// 
//  Dependencies:
//  Depends on cmm script framework (various dependent files) to work, and assumes that a 
//  sanitized argument line has been passed in from std_loadsim.cmm script
//  
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 07/25/2016 c_sknven	    Modified Script to Support Eldarion Pro DDR specs
// 02/11/2016 c_akaki       Added postmortem script call
// 08/03/2015 JBILLING      Added support for automation and error message passing
// 08/03/2015 JBILLING      Add SSR functionality
// 04/16/2015 JBILLING      Updated loadsim command
// 11/07/2014 JBILLING      Overhaul with new mmu sequence and error messages
// 10/02/2013 AJCheriyan    Change for newer SW installations
// 09/06/2012 AJCheriyan    Added USB RAM dump support
// 07/10/2012 AJCheriyan    Created for B-family 
//




///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// std_loadsim_mpss //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


//###################Arguments passed #####################
LOCAL &ArgumentLine
ENTRY %LINE &ArgumentLine
LOCAL &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
ENTRY &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11


//#####################Select Subroutine###################
// Name of the utility we are calling
LOCAL &SUBROUTINE
// Any subroutine specific options
// Default exists for each subroutine


// Input Argument 0 is the name of the utility
&SUBROUTINE="&UTILITY"
IF !(("&SUBROUTINE"=="VERIFYBUILD")||("&SUBROUTINE"=="HELP")||("&SUBROUTINE"=="help"))
(
    &SUBROUTINE="MAIN"   
)

    // This should be created by some top level script. The setupenv for each proc would
    // set this up
    AREA.SELECT
    // Call the required utility
    GOSUB &SUBROUTINE &ArgumentLine
    LOCAL &rvalue
    ENTRY %LINE &rvalue

    ENDDO &rvalue



////////////////////////////////////////
//
//            MAIN
//            Main std_loadsim_mpss logic
//            Expected input: None. Relies on global variables
//
/////////////////////////////////////////

MAIN:
    LOCAL &image &imagebuildroot &logpath &logtype &targetprocessor &processor_root_name &loadsimscript &symbolloadscript &multi_elf_option &alternateelf &extraoptions 
    ENTRY &image &imagebuildroot &logpath &logtype &targetprocessor &processor_root_name &loadsimscript &symbolloadscript &multi_elf_option &alternateelf &extraoptions 

    LOCAL &rvalue
    
    // Load the memory map to initialize variables
    do std_memorymap 

    // First, all the sanity checks
    GOSUB CHECKBINARIES
        ENTRY %LINE &rvalue
        IF "&rvalue"!="SUCCESS"
        (
            GOSUB EXIT &rvalue
        )
        
    // Binaries look good. Else, we wouldn't be here    
    GOSUB SETUPSIM
    
    // Load the binaries
    GOSUB LOADBIN
    
    // Setup the environment
    do std_setupenv noareaclear
    
    // Load the symbols
    do std_loadsyms_mpss &imagebuildroot locally NULL NULL &alternateelf &extraoptions
        ENTRY %LINE &rvalue
        IF "&rvalue"!="SUCCESS"
        (
            GOSUB EXIT &rvalue    
        )
        
    
    // Load the "state" at the time of the crash
    GOSUB RESTORESTATE
    ENTRY %LINE &rvalue
		IF "&rvalue"!="SUCCESS"
        (
            GOSUB EXIT &rvalue    
        )
        
    GOSUB POSTMORTEM_ANALYSIS
    ENTRY %LINE &rvalue
    
    //Wait for a few seconds if in automation mode  
    //in case user wants to spot check results.
    IF STRING.SCAN("&extraoptions","forcesilent",0)!=-1
    (
        WAIT.3s
    )
    // Off you go..
    GOSUB EXIT &rvalue

////////////////////////////////////////
//
//          VERIFYBUILD
//          Public function
//          Verify that needed files are present
//          Expected input: Build Location
//
/////////////////////////////////////////
VERIFYBUILD:
    LOCAL &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10
    ENTRY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10
    LOCAL &result &LOCAL_BUILDROOT
    
    &result="SUCCESS"
    &LOCAL_BUILDROOT="&IARG0"
    PRINT "Checking that needed files exist"
    
    AREA.RESET
    AREA.CREATE std_loadsim_build_verify 125. 6.
    AREA.SELECT std_loadsim_build_verify
    WINPOS 0. 0. 80. 6.
    AREA.VIEW std_loadsim_build_verify
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/products/scripts/&MODEM_BUILDID/std_extensions.cmm")
    (
        PRINT "Found std_extensions.cmm"
    )
    ELSE
    (
        PRINT %ERROR "Missing core/products/std_extensions.cmm. Context loading will fail"
        &result="FAILURE_VERIFYBUILD_MPSS"
    )
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/products/scripts/&MODEM_BUILDID/std_toolsconfig.cmm")
    (
        PRINT "Found std_toolsconfig.cmm"
    )
    ELSE
    (
        PRINT %ERROR "Missing core/products/std_toolsconfig.cmm. Context loading will fail"
        &result="FAILURE_VERIFYBUILD_MPSS"
    )
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/products/scripts/&MODEM_BUILDID/std_scripts.men")
    (
        PRINT "Found std_scripts.men"
    )
    ELSE
    (
        PRINT %ERROR "Missing core/products/std_scripts.men. Context loading will fail"
        &result="FAILURE_VERIFYBUILD_MPSS"
    )
    
    IF FILE.EXIST("&LOCAL_BUILDROOT/&processor_root_name/core/debugtools/err/cmm/load_coredump.cmm")
    (
        PRINT "Found load_coredump.cmm"
    )
    ELSE
    (
        PRINT "Missing core/debugtools/err/cmm/load_coredump.cmm. Some context may be lost!"
        //&result="FAILURE_VERIFYBUILD_MPSS"
    )

    IF ("&result"=="SUCCESS")
    (
        PRINT "Verification success: Found needed context files"
        PRINT " "
    )
    ELSE
    (
        GOSUB FATALEXIT "Error: Needed build files (std_extensions, std_scripts, std_toolsconfig) not found"
    )

    
    

    RETURN &result
////////////////////////////////////////
//
//          SETUPSIM
//          Private function
//          Set the simulator for the processor we want
//          Expected input: None
//
/////////////////////////////////////////
SETUPSIM:
    SYS.CPU HEXAGONV56
    SYS.UP
    

    RETURN

    
////////////////////////////////////////
//
//          CHECKBINARIES
//          Private function
//          Checks if the binaries for the system are present in the location
//          Loglocation should not be empty and assumes memory map is loaded
//          Expected input: None. Uses global variables
//          &logtype=<AUTODETECT,JTAG,USB> 
//
/////////////////////////////////////////
CHECKBINARIES:
LOCAL &file1 &file2 &logclass

    
    // This is the best place to determine the type of the log too
    IF ("&logtype"=="AUTODETECT")
    (
    
                &logclass="&logtype"
                
                //Check files present and try and auto-assign.
                IF ("&logclass"!="SSR")
                (
                    // Check for SSR logs
                    // User should have given full path to ssr file. 
                    IF (FILE.EXIST(&logpath))
                    (
                        &filetype=FILE.TYPE(&logpath)
                        IF ("&filetype"=="ELF")||("&filetype"=="BINARY")
                        (
                            &logclass="SSR"
                        )
                    )
                )

                IF ("&logclass"=="AUTODETECT")
                (
                    // Check for USB logs
                    do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
                    ENTRY &file1

                    IF ("&file1"=="TRUE")
                    (
                        &logclass="USB"
                    )
                )
                
                IF ("&logclass"=="AUTODETECT")
                (
                    // Check for JTAG logs
                    do std_utils FILEXIST EXIT &logpath &MPSS_SW_log
                    ENTRY &file1

                    IF ("&file1"=="TRUE")
                    (
                         &logclass="JTAG"
                    )
                )
                
                
                // If we even after that, we are stuck with "AUTODETECT" we have a problem
                IF ("&logclass"=="AUTODETECT")
                (
                    AREA
                    PRINT %ERROR "USB/SSR/JTAG logs not found in folder: &logpath. If using SSR type log, please give full path and filename.'do std_loadsim help' for more"
                    GOSUB FATALEXIT "Failed to auto-detect log type. Unexpected file names or types in &logpath"
                )
                ELSE
                (
                    // Safe to change the logtype
                    &logtype="&logclass"
                    PRINT "Detected &logtype logs in: &logpath"
                )
    )
            
    IF ("&logtype"=="JTAG")
    (
        
        
        // Check for JTAG logs
        do std_utils FILEXIST EXIT &logpath &MPSS_SW_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            AREA
            PRINT %ERROR "JTAG MPSS logs not present in folder: &logpath"
            PRINT %ERROR "If you have only a binary, provide address"
            PRINT %ERROR "of MPSS's DDR location when binary collected, via command line. e.g.: "
            PRINT %ERROR "do std_loadsim Img=mpss Bld=C:\my\build Log=C:\temp\mylog\binary.bin extraoption=physaddr->0x8F800000"
            PRINT %ERROR "Type 'do std_loadsim help' for more information"
            GOSUB FATALEXIT "JTag logs not found in &logpath"
            
        )
        //If user has specified jtag logs and provided a physical address,
        //Then use the logic from SSR mode. If they haven't provided a
        //physical address, then look for Shared Imem log. If it's not there, exit.
        IF STRING.SCAN("&extraoptions","physaddr->",0)!=-1
        (
            &logpath="&logpath/&MPSS_SW_log"
            &logtype="SSR"
        )
        ELSE
        (
            // Check for Shared IMEM logs
            do std_utils FILEXIST EXIT &logpath &SHARED_IMEM_log
            ENTRY &file1

            IF ("&file1"=="FALSE")
            (
                AREA
                PRINT %ERROR "JTAG Shared IMEM logs not present in folder: &logpath"
                PRINT %ERROR "If you have only a binary, provide address"
                PRINT %ERROR "of MPSS's DDR location when binary collected, via command line. e.g.: "
                PRINT %ERROR "do std_loadsim Img=mpss Bld=C:\my\build Log=C:\temp\mylog\binary.bin extraoption=physaddr->0x8F800000"
                PRINT %ERROR "Type 'do std_loadsim help' for more information"
                GOSUB FATALEXIT "Shared IMEM logs not found in &logpath, for log type &logtype"
            )
        )

    )
    
    IF ("&logtype"=="USB")
    (
        
        // Check for USB logs
        do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            AREA
            PRINT %ERROR "USB MPSS logs not present in folder: &logpath. (If trying ot use SSR log type, please provide full path and filename. 'do std_loadsim help' for more)"
            GOSUB FATALEXIT "USB logs not found in &logpath, for log type &logtype"
        )
        

        // Check for USB logs
        do std_utils FILEXIST EXIT &logpath &OCIMEM_USB_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            PRINT %ERROR "USB Shared IMEM logs not present in folder: &logpath"
            GOSUB FATALEXIT "Shared IMEM logs not found in &logpath, for log type &logtype"
        )
        
        
    )
    
    IF ("&logtype"=="SSR")
    (
        LOCAL &filetype
        // Check for SSR logs
        IF !(FILE.EXIST(&logpath))
        (
            AREA
            PRINT %ERROR "Not able to access SSR MPSS logs at: &logpath. If using SSR type log, please give full path and filename. 'do std_loadsim help' for more"
            GOSUB FATALEXIT "SSR logs not found at &logpath, for log type &logtype"
            
        )
        
        &filetype=FILE.TYPE(&logpath)
        
        IF ("&filetype"!="ELF")
        (
            IF ("&filetype"=="BINARY")
            (
        
                IF STRING.SCAN("&extraoptions","physaddr->",0)==-1
                (
                
                        WINPOS 37% 37% 85. 15.
                        AREA.CREATE A0003
                        AREA.SELECT A0003
                        AREA.VIEW A0003
                        //AREA.RESET
                        //AREA
                        PRINT %ERROR "Binary dump file type specified for dump file, but no 'physaddr->' specified "
                        PRINT %ERROR "in extraoption. Please specify a physaddr->0x<physical_base> in command"
                        PRINT %ERROR "line for  binary standalone load. Note that binary standalone loading is only "
                        PRINT %ERROR "available from command line at this time"
                        PRINT " "
                        PRINT %ERROR "Type 'do std_loadsim help' for additional details"
                        PRINT " "
                        PRINT " "
                        PRINT " "
                        AREA.SELECT A000
                        AREA.VIEW A000
                        GOSUB FATALEXIT "physical address needed for logtype &logtype and filetype &filetype. See help menu"

                
                )
            )
            ELSE
            (
                PRINT %ERROR "Wrong Filetype for SSR Log file: &logpath. Expected: ELF or BIN, got &filetype"
                GOSUB FATALEXIT "Wrong Filetype for SSR Log file: &logpath. Expected: ELF or BIN, got &filetype"
            )
        )
        
    )

    RETURN SUCCESS

////////////////////////////////////////
//
//          LOADBIN
//          Private function
//          Loads the saved binaries
//          Expected input: None. Uses global variables
//          &logtype=<JTAG,USB> 
//
/////////////////////////////////////////

LOADBIN:
LOCAL &Bin &DDR_start
	
    IF ("&logtype"=="JTAG")
    (
        do std_utils LOADBIN &logpath &SHARED_IMEM_log &SHARED_IMEM_start
        
        // Load the memory map again for relocated images
        do std_memorymap DYNAMIC
        
        // Now load the logs
        do std_utils LOADBIN &logpath &MPSS_SW_log &MPSS_SW_start
    )
    ELSE IF ("&logtype"=="USB")
    (
        // Load the shared IMEM logs
        do std_utils LOADBIN &logpath &OCIMEM_USB_log &OCIMEM_start
        
        // Load the memory map again for relocated images
        do std_memorymap DYNAMIC

        //FOR 8976 different different ddr sizes are present.  
        //for 2GB DDR DDRCS0 is loading at  &DDR_1_start
        //for 3GB DDR DDRCS1 is loading at &DDR_1_start. so workaround for this
	//for 6GB DDR, DDR sizes are specified as DDRCS0(3GB) starts from 0x40000000(&6GB_DDR_1_start) to 0x100000000(GLOBAL &6GB_DDR_1_end), DDRCS1_0(1GB), DDRcs1_2(1GB) & DDRCS1_3(1GB)
	//To support this configuration, following workaround has been implemented
	//ddr_select script returns two arguments: DDR binary file name & DDR start address
		
        do ddr_select &logpath 
        ENTRY &Bin &DDR_start 
        
        //Load the binary. 
        //skip memory up to start of subsystem software dump, subtracting start of ddr region start
	IF ("&Bin"=="DDRCS0")&&("&DDR_start"=="&DDR_1_start")
        (
		do std_utils LOADBIN &logpath &DDR_1_USB_log &MPSS_SW_start &MPSS_SW_start&(~&DDR_1_start) &MPSS_SW_size
        )
	// Loading the binary for 6GB DDR 
	ELSE IF ("&Bin"=="DDRCS0")&&("&DDR_start"=="&6GB_DDR_1_start")
        (
		do std_utils LOADBIN &logpath &6GB_DDR_1_USB_log &MPSS_SW_start ((&MPSS_SW_start)-(&6GB_DDR_1_start)) &MPSS_SW_size
        )
	ELSE 
        (
		do std_utils LOADBIN &logpath &DDR_2_USB_log &MPSS_SW_start &MPSS_SW_start&(~&DDR_1_start) &MPSS_SW_size
        )
    )
    ELSE IF ("&logtype"=="SSR")
    (
        
        
        IF FILE.TYPE(&logpath)=="ELF"
        (
            // Load the memory map again for relocated images
            //do std_memorymap DYNAMIC
            ON ERROR CONTINUE
            &tempdir=OS.ENV(TEMP)
            &tempfile="&tempdir\datalogfile.txt"
            PRINTER.FILE &tempfile
            PRINTER.FILETYPE ASCII
            PRINTER.SIZE 0XFA, 0XFA
            
            
            SYS.LOG.INIT
            SYS.LOG.RESET
            SYS.LOG.SIZE 10000.
            SYS.LOG.ON
            
            //DATA.LOG
            DATA.LOAD.ELF &logpath /noclear
            WP.DATA.LOG
                    
            SYS.LOG.OFF
            
            LOCAL &linelst &index
            open #1 &tempfile
            read #1 %line &linelst
            read #1 %line &linelst
            read #1 %line &linelst
            close #1
            &index=string.scan("&linelst","P:",0)
            
            &address=string.mid("&linelst",&index+2,8.)
            &MPSS_SW_start=ADDRESS.OFFSET(P:&address)
            ON ERROR
            PRINT "Detected from &logpath that MPSS SW start is at &MPSS_SW_start"
        )
        ELSE IF FILE.TYPE(&logpath)=="BINARY"
        (
            IF STRING.SCAN("&extraoptions","physaddr->",0)==-1
            (
                PRINT %ERROR "Binary dump file specified for SSR file, but no 'physaddr->' specified "
            )
            ELSE
            (
                &index=string.scan("&extraoptions","physaddr->",0)
                
                &address=string.mid("&extraoptions",&index+10.,8.)
                //If '0x' specified, shift address up. Else leave it.
                IF STRING.SCAN(STRING.UPR("&address"),"0X",0)!=-1
                (
                    &address=&address*0x100
                )
                &MPSS_SW_start=ADDRESS.OFFSET(P:&address)
                PRINT "Setting MPSS Start address as &MPSS_SW_start per extraoption specified"
                DATA.LOAD.BINARY &logpath &MPSS_SW_start
            )
        
        
        )
        ELSE
        (
            //Never should get here
            PRINT %ERROR "Unrecognized filetype. expected BIN or ELF"
        )
    )
    RETURN
    
////////////////////////////////////////
//
//          RESTORESTATE
//          Private function
//          To load the error information from the saved logs
//          Expected input: None. Uses global variables
//          &logtype=<JTAG,USB> 
//          Expects various files to be present
//
/////////////////////////////////////////
RESTORESTATE:

            // Symbols should be loaded prior to this step
            
            LOCAL &VIRT_START
            ON ERROR GOSUB 
            (
                    IF !SYMBOL.EXIST(start)
                    (
                        PRINT %ERROR "Symbol 'start' not defined. Error with symbol loading ocurred. Context loading may fail"
                    )
                    &VIRT_START=0xC0000000
                    GOTO SKIP_PAGETABLE_LOAD
            )
            &VIRT_START=ADDRESS.OFFSET(start)

            // Restore saved TCM
            DATA.COPY (qurt_tcm_dump-&VIRT_START+&MPSS_SW_start)++v.value(qurt_tcm_dump_size) d.l(QURTK_l2tcm_base)

            
            
            //Format MMU to QuRT Pagetables
            IF SYMBOL.EXIST(QURTK_page_table_v2)
            (
            
                //FIXME - needed simulator build version should go in std_versionchecker.cmm
                LOCAL &MinimumT32_QURTV2_Sim_Build
                &MinimumT32_QURTV2_Sim_Build=62625.
                &T32build=SOFTWARE.BUILD()
                IF (&T32build<&MinimumT32_QURTV2_Sim_Build)
                (
                        WINPOS 37% 37% 85. 15.
                        AREA.CREATE A0002
                        AREA.SELECT A0002
                        AREA.VIEW A0002
                        //AREA.RESET
                        //AREA
                        PRINT %ERROR "   Error! Scripts environment detects that QuRT"
                        PRINT %ERROR "   is using QURTV2 pagetables but T32 version"
                        PRINT %ERROR "   is too old to support it. Please use T32 build"
                        PRINT %ERROR "   version of &MinimumT32_QURTV2_Sim_Build or newer (April 28th 2015 or newer)"
                        PRINT %ERROR "   Attempting to load context from TLB's, but context loading"
                        PRINT %ERROR "   and QuRT buttons will likely fail to work."
                        PRINT " "
                        PRINT " "
                        PRINT " "
                        AREA.SELECT A000
                        AREA.VIEW A000
                        //If in automation mode, fatal exit. Else keep trying.
                        IF STRING.SCAN("&extraoptions","forcesilent",0)==-1
                        (
                            GOTO SKIP_PAGETABLE_LOAD
                        )
                        ELSE
                        (
                            GOSUB FATALEXIT "Invalid T32 version used with QURTV2 pagetables"
                        )
                )
                
                &ABS_QURTK_pagetables=(QURTK_page_table_v2-&VIRT_START+&MPSS_SW_start)
                //MMU.FORMAT QURTV2 &ABS_QURTK_pagetables &VIRT_START++&MPSS_SW_size &MPSS_SW_start
                &v_pgt=ADDRESS.OFFSET(D:QURTK_page_table_v2)&0xFFF00000
                &p_pgt=(&v_pgt-&VIRT_START+&MPSS_SW_start)
                MMU.FORMAT QURTV2 QURTK_page_table_v2 &v_pgt++0xFFFFF &p_pgt
                
            )
            ELSE
            (
                &ABS_QURTK_pagetables=(QURTK_pagetables-&VIRT_START+&MPSS_SW_start)
                MMU.FORMAT QURT d.l(&ABS_QURTK_pagetables) &VIRT_START++&MPSS_SW_size &MPSS_SW_start
            )
            
            MMU.ON
            MMU.SCAN KernelPageTable
            
SKIP_PAGETABLE_LOAD:
            ON ERROR
            GOSUB RESTORE_TLB
            //needs to be done after TLB loaded
            DATA.COPY (qurt_tcm_dump-&VIRT_START+&MPSS_SW_start)++v.value(qurt_tcm_dump_size) d.l(QURTK_l2tcm_base)
            
            GOSUB DUMP_STATE_ANALYSIS
            ENTRY %LINE &rvalue
            
                  
            
            

    RETURN &rvalue
    
////////////////////////////////////////
//
//          RESTORE_TLB
//          Private function
//          Scan and sync through TLB dumps 
//          Expected input: None. Uses global variables
//          Needs TLB dump to be populated or will print error
//
/////////////////////////////////////////
RESTORE_TLB:
        
        //Restore TLB state
        IF (Y.EXIST(QURTK_tlb_dump))
        (
            IF (V.VALUE((int)QURTK_tlb_dump)!=0x0)
            (
                &tlb_idx=0
                // 128 entries in the TLB for QDSP6V5A,H,128_A
                REPEAT 128
                (
                     &val0=V.VALUE(((unsigned int *)&QURTK_tlb_dump)[2*&tlb_idx])
                    &val1=V.VALUE(((unsigned int *)&QURTK_tlb_dump)[2*&tlb_idx+1])
                    MMU.SET TLB &tlb_idx &val0 &val1
                    &tlb_idx=&tlb_idx+1
                )
                MMU.TLB.SCAN
            )
            ELSE
            (
                PRINT %ERROR "Warning! TLB Dump empty. Some context may be lost or menus may not work properly"
            )
        )
RETURN

////////////////////////////////////////
//
//          RESTORE_REGISTER_CONTEXT
//          Private function
//          Restore register context from existing dump files 
//          Expected input: None. Uses global variables
//          Needs script files to be present, or will throw error
//
/////////////////////////////////////////
RESTORE_REGISTER_CONTEXT:
LOCAL &logtype
ENTRY &logtype
    IF ("&logtype"=="USB")
    (
            &runscript="true"
            ON ERROR GOSUB
            (
                    PRINT "Warning! Error ocurred during load_coredump. Some information may be lost."
                    &runscript="false"
            )
            IF ("&runscript"=="true")
            (
                //coredump not always populated
                do std_utils EXECUTESCRIPT EXIT &imagebuildroot/&processor_root_name/core/debugtools/err/cmm/load_coredump.cmm
            )
            ON ERROR
            
           
    )
    

    // Restore the registers from file if JTAG logs
    IF ("&logtype"=="JTAG")
    (
        THREAD 0
        do std_utils EXECUTESCRIPT EXIT &logpath/&MPSS_Thread0_regs
        THREAD 1
        do std_utils EXECUTESCRIPT EXIT &logpath/&MPSS_Thread1_regs
        THREAD 2
        do std_utils EXECUTESCRIPT EXIT &logpath/&MPSS_Thread2_regs
      //  THREAD 3
      //  do std_utils EXECUTESCRIPT EXIT &logpath/&MPSS_Thread3_regs

    )
    
    RETURN
    


////////////////////////////////////////
//
//          DSP_LOAD_PAGETABLES
//          Private function
//          Prints out entire pagetable and creates translation entries upon reading in
//          Not used in current implementation 
//          Expected input: None. Uses global variables
//          Needs dump to be good and symbols to be correct, or will throw error
//
/////////////////////////////////////////
DSP_LOAD_PAGETABLES:

            LOCAL &linelst &vpage &ppage &size &vend &pend &result
            //&loc="c:\temp"
            // moved to up layer because one mapping needs deletion
            // printer.file &loc\DSP_pgt.txt
            // printer.filetype ASCII 
            // printer.size 0xfa, 0xfa
            // wp.task.pgt
            &result="SUCCESS"
            
            open #1 c:\temp\DSP_pgt.txt
            read #1 %line &linelst
            read #1 %line &linelst
            read #1 %line &linelst
start:
            read #1 %line &linelst 
            &vpage=string.mid("&linelst", 0, 7)
            if (("&vpage"=="B::task")||("&vpage"=="Pagetab")||("&vpage"=="VPage  ")||("&vpage"=="PPage  "))
            (
                goto start
            )

            if ("&vpage"=="       ")
            (
                goto done_reading
            )
            
            if "&vpage"==""
            (
                goto done_reading
            )
            &ppage=string.mid("&linelst",0xa, 7)
            if "&ppage"==""
            (
                goto done_reading
            )
            
            if (STRING.SCAN("&ppage","******",0)!=-1)
            (
                PRINT "Warning! Pagetable dumps invalid. May be a bad dump. Context loading could be limited"
                &result="FAILURE"
                GOTO done_reading
            )
            &size=string.mid("&linelst", 0x14, 8)
            if "&size"==""
            (
                goto done_reading
            )
            
            // print "&vpage"+" "+"&ppage"+" "+"&size"
            &vpage=&vpage<<0xc
            &ppage=&ppage<<0xc
            &vend=&vpage+&size
            &pend=&ppage+&size
            mmu.create &vpage--&vend &ppage--&pend
            goto start
done_reading:
            close #1

RETURN &result

////////////////////////////////////////
//
//          DUMP_STATE_ANALYSIS
//          Private function
//          Examines key variables used in QuRT menu plugins to determine symbol mismatch
//          Expected input: None. Uses global variables and ELF symbols
//          Needs dump to be good and symbols to be correct, or will throw error
//
/////////////////////////////////////////
DUMP_STATE_ANALYSIS:

        LOCAL &qurt_state_results &rvalue
        &qurt_state_results=1

        IF ((!Y.EXIST(QURTK_CONTEXT_SIZE))||(!Y.EXIST(QURTK_MAX_THREADS))||(!Y.EXIST(qurt_has_initted))||(!Y.EXIST(QURTK_flush_cache_status)))
        (
            PRINT %ERROR "QuRT variables not known. Error with ELF file. Exiting"
            GOSUB FATALEXIT "QuRT variables not known. Error with ELF file. Exiting"
        )
        
        //FIXME these should be set in a library of some sort
        //These three variables needed for QuRT threads.
        IF ((v.value(QURTK_CONTEXT_SIZE)<240.)||(v.value(QURTK_CONTEXT_SIZE)>600.))
        (
            PRINT %ERROR "Warning - QURTK_CONTEXT_SIZE is an unexpected value. Could be a symbol mismatch issue. QuRT menu extensions may not run"
            &qurt_state_results=0
        )
        IF ((v.value(QURTK_MAX_THREADS)<10.)||((v.value(QURTK_MAX_THREADS)>600.)))
        (
            PRINT %ERROR "Warning - QURTK_MAX_THREADS is an unexpected value. Could be a symbol mismatch issue. QuRT menu extensions may not run"
            &qurt_state_results=0
        )
        IF (v.value(qurt_has_initted)!=1.)
        (
            PRINT %ERROR "Warning - qurt_has_initted is not equal to 1. Could be a symbol mismatch issue. QuRT menu extensions may not run"
            &qurt_state_results=0
        )
        
        &DumpState=v.value((int)QURTK_flush_cache_status)
        IF ((&DumpState==0x02)||(&DumpState==0x03))
        (
            PRINT %ERROR "Warning! QURTK_flush_cache_status is &DumpState. Cache is flushed, but TCM and L2VIC were not saved"
            &qurt_state_results=0
        )
        
        IF (&qurt_state_results==0x1)
        (
            PRINT "Qurt State variable verification successful."
            &rvalue="SUCCESS"
        )
        ELSE
        (
            &rvalue="FAILURE - Qurt state verification failed. Possible symbol mismatch."
        )
  
        WINPOS 34% 63% 35% 41% 0. 0. W005
        task.TaskList


        RETURN &rvalue

POSTMORTEM_ANALYSIS:
    
    GOSUB RESTORE_REGISTER_CONTEXT &logtype    
    
    //Execute registered postmortem scripts
    do std_utils EXECUTESCRIPT EXIT &imagebuildroot/&processor_root_name/core/products/scripts/&MODEM_BUILDID/std_postmortem.cmm    
    
    ENDDO SUCCESS

BADDUMP:
    PRINT "Unrecoverable errors ocurred during dump. Cannot restore context"
    GOSUB FATALEXIT "Unrecoverable errors ocurred during dump. Cannot restore context"
    
EXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    ENDDO &rvalue
    

//Should never get here. 
FATALEXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    IF STRING.SCAN("&FAILUREKEYWORD","FAILUREKEYWORD",0)==-1
    (
        GOSUB EXIT &FAILUREKEYWORD - &rvalue
    )
    ELSE
    (
        GOSUB EXIT &rvalue
    )
    
//Should never get here
    END
