//============================================================================
//  Name:                                                                     
//    std_loadsim_apps.cmm 
//
//  Description:                                                              
//    Script to load APPS logs
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//  Description:
//  T32 simulator loader specific to rpm dumps. This file has some 8994 specific areas.
// 
//  Dependencies:
//  Depends on cmm script framework (various dependent files) to work, and assumes that a 
//  sanitized argument line has been passed in from std_loadsim.cmm script
//  
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who             what, where, why
// --------   ---             ---------------------------------------------------------
// 07/13/2015 c_gunnan      Created for 8976
// 11/07/2014 JBILLING      Overhaul with new error messages
// 09/06/2012 AJCheriyan    Added USB RAM dump support
// 08/31/2012 AJCheriyan    Created for B-family 
//




///////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////// std_loadsim_rpm //////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


//###################Arguments passed #####################
LOCAL &ArgumentLine
ENTRY %LINE &ArgumentLine
LOCAL &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
ENTRY &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11


//#####################Select Subroutine###################
// Name of the utility we are calling
LOCAL &SUBROUTINE
// Any subroutine specific options
// Default exists for each subroutine


// Input Argument 0 is the name of the utility
&SUBROUTINE="&UTILITY"
IF !(("&SUBROUTINE"=="VERIFYBUILD")||("&SUBROUTINE"=="HELP")||("&SUBROUTINE"=="help"))
(
    &SUBROUTINE="MAIN"   
)

    // This should be created by some top level script. The setupenv for each proc would
    // set this up
    AREA.SELECT
    // Call the required utility
    GOSUB &SUBROUTINE &UTILITY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10 &IARG11
    ENTRY &PASS &RVAL0 &RVAL1 &RVAL2

    ENDDO &PASS &RVAL0 &RVAL1 &RVAL2



////////////////////////////////////////
//
//            MAIN
//            Main std_loadsim_adsp logic
//            Expected input: None. Relies on global variables
//
/////////////////////////////////////////

MAIN:
    LOCAL &image &imagebuildroot &logpath &logtype &targetprocessor &processor_root_name &loadsimscript &symbolloadscript &multi_elf_option &extraoptions 
    ENTRY &image &imagebuildroot &logpath &logtype &targetprocessor &processor_root_name &loadsimscript &symbolloadscript &multi_elf_option &extraoptions 


    // Load the memory map to initialize variables
    do std_memorymap 

    // Setup the environment
    do std_setupenv

    // First, all the sanity checks
    GOSUB CHECKBINARIES
    
    // Binaries look good. Else, we wouldn't be here    
    GOSUB SETUPSIM
    
    // Load the binaries
    GOSUB LOADBIN

    // Load the symbols
    // do std_loadsyms_apps &imagebuildroot locally

    // Load the "state" at the time of the crash
    GOSUB RESTORESTATE

    // Off you go..
    GOTO EXIT

////////////////////////////////////////
//
//          VERIFYBUILD
//          Public function
//          Verify that needed files are present
//          Expected input: Build Location
//
/////////////////////////////////////////
VERIFYBUILD:
    LOCAL &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10
    ENTRY &OPTION &IARG0 &IARG1 &IARG2 &IARG3 &IARG4 &IARG5 &IARG6 &IARG7 &IARG8 &IARG9 &IARG10
    LOCAL &result &LOCAL_BUILDROOT
    
    &result="SUCCESS"

    RETURN &result
////////////////////////////////////////
//
//          SETUPSIM
//          Private function
//          Set the simulator for the processor we want
//          Expected input: None
//
/////////////////////////////////////////
SETUPSIM:
   SYS.CPU CORTEXA53
	SYS.UP
    

    RETURN

    
////////////////////////////////////////
//
//          CHECKBINARIES
//          Private function
//          Checks if the binaries for the system are present in the location
//          Loglocation should not be empty and assumes memory map is loaded
//          Expected input: None. Uses global variables
//          &logtype=<AUTODETECT,JTAG,USB> 
//
/////////////////////////////////////////
CHECKBINARIES:
LOCAL &file1 &file2 &file3 &logclass


    // This is the best place to determine the type of the log too
    IF ("&logtype"=="AUTODETECT")
    (
    
        &logclass="&logtype"
        
        
        IF ("&logclass"!="USB")
        (
            // Check for USB logs
            do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
                ENTRY &file1

            IF ("&file1"=="TRUE")
            (
                 &logclass="USB"
            )
        )

        IF ("&logclass"!="JTAG")
        (
            do std_utils FILEXIST EXIT &logpath &HLOS_1_log
                ENTRY &file1


            IF ("&file1"=="TRUE")
            (
                 &logclass="JTAG"
            )
        )



        // If we even after that, we are stuck with "AUTODETECT" we have a problem
        IF ("&logclass"=="AUTODETECT")
        (
            PRINT %ERROR "Neither USB nor JTAG logs present in folder: &logpath"
            GOTO FATALEXIT
        )
        ELSE
        (
            // Safe to change the logtype
            &logtype="&logclass"
            PRINT "Detected &logtype logs in folder: &logpath"
        )
    )
            
    IF ("&logtype"=="JTAG")
    (
        
        // Check for JTAG logs
        do std_utils FILEXIST EXIT &logpath &HLOS_1_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            PRINT %ERROR "JTAG logs not present in folder: &logpath"
            GOTO FATALEXIT
        )
    )

    IF ("&logtype"=="USB")
    (
        
        // Check for USB logs
        do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
        ENTRY &file1

        IF ("&file1"=="FALSE")
        (
            PRINT %ERROR "USB logs not present in folder: &logpath"
            GOTO FATALEXIT
        )
    )    
        
        
    

    RETURN

////////////////////////////////////////
//
//          LOADBIN
//          Private function
//          Loads the saved binaries
//          Expected input: None. Uses global variables
//          &logtype=<JTAG,USB> 
//
/////////////////////////////////////////
LOADBIN:
    
    LOCAL &filepresent
	LOCAL &Bin

    IF ("&logtype"=="JTAG")
    (

        do std_utils FILEXIST EXIT &logpath &HLOS_1_log
            ENTRY &filepresent
        IF ("&filepresent"=="TRUE")
        (
             do std_utils LOADBIN &logpath &HLOS_1_log &HLOS_1_start
        )

        do std_utils FILEXIST EXIT &logpath &HLOS_2_log
            ENTRY &filepresent
        IF ("&filepresent"=="TRUE")
        (
             do std_utils LOADBIN &logpath &HLOS_2_log &HLOS_2_start
        )

        do std_utils FILEXIST EXIT &logpath &HLOS_3_log
            ENTRY &filepresent
        IF ("&filepresent"=="TRUE")
        (
             do std_utils LOADBIN &logpath &HLOS_3_log &HLOS_3_start
        )
    )

        
    IF ("&logtype"=="USB")
    (
 
		//for 2GB DDR DDRCS0 is loading at  &DDR_1_start
		//for 3GB DDR DDRCS1 is loading at &DDR_1_start. so workaround for this
		do ddr_select &logpath 
		ENTRY &Bin
		IF ("&Bin"=="DDRCS0")
		(
				do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
				ENTRY &filepresent
				IF ("&filepresent"=="TRUE")
				(
					do std_utils LOADBIN &logpath &DDR_1_USB_log &DDR_1_start
				)

					do std_utils FILEXIST EXIT &logpath &DDR_2_USB_log
					ENTRY &filepresent
				IF ("&filepresent"=="TRUE")
				(
				do std_utils LOADBIN &logpath &DDR_2_USB_log &DDR_2_start
				)
		)
		
		ELSE
		(
				do std_utils FILEXIST EXIT &logpath &DDR_1_USB_log
				ENTRY &filepresent
				IF ("&filepresent"=="TRUE")
				(
					do std_utils LOADBIN &logpath &DDR_1_USB_log &3GB_DDR_1_start
				)

					do std_utils FILEXIST EXIT &logpath &DDR_2_USB_log
					ENTRY &filepresent
				IF ("&filepresent"=="TRUE")
				(
				do std_utils LOADBIN &logpath &DDR_2_USB_log &3GB_DDR_2_start
				)
		)
		
		
    )
    
    RETURN
    
////////////////////////////////////////
//
//          RESTORESTATE
//          Private function
//          To load the error information from the saved logs
//          Expected input: None. Uses global variables
//          &logtype=<JTAG,USB> 
//          Expects various files to be present
//
/////////////////////////////////////////
RESTORESTATE:
    // Restore the registers from file if JTAG logs
    IF ("&logtype"=="JTAG")
    (
         do std_utils EXECUTESCRIPT EXIT &logpath/&APPS_Cluster0_Core0_regs
         do std_utils EXECUTESCRIPT EXIT &logpath/&APPS_Cluster0_Core0_mmu
    )
	// Need to fix once getting the ramdump for feero(8937)
	Register.Set NS 1
	Data.Set SPR:0x30201 %Quad 0x2007d000
	Data.Set SPR:0x30202 %Quad 0x00000032B5193519
	Data.Set SPR:0x30A20 %Quad 0x000000FF440C0400
	Data.Set SPR:0x30A30 %Quad 0x0000000000000000
	Data.Set SPR:0x30100 %Quad 0x0000000004C5D93D
	Register.Set CPSR 0x3C5
	MMU.Delete
	MMU.SCAN PT 0xFFFFFF8000000000--0xFFFFFFFFFFFFFFFF
	mmu.on	
	mmu.pt.list 0xffffff8000000000


    RETURN

FATALEXIT:
    END

EXIT:
    ENDDO

    
