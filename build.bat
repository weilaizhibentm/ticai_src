@echo off

call env.bat

if /i "%1" == "modem" (
  cd MPSS.TA.2.3/modem_proc/build/ms
  if /i "%2" == "" ( 
    build.cmd 8953.genw3k.prod -k
  ) else (
    build.cmd 8953.genw3k.prod %~2 %~3 %~4 %~5 %~6 %~7 %~8 %~9
  )
  cd ../../../../
) else if /i "%1" == "boot" (
  cd BOOT.BF.3.3/boot_images/build/ms
  if /i "%2" == "" ( 
  build.cmd TARGET_FAMILY=8953 --prod  -j8
  ) else (
  build.cmd TARGET_FAMILY=8953 --prod  %~2
  )
  cd ../../../../
) else if /i "%1" == "rpm" (
  cd RPM.BF.2.4/rpm_proc/build
  if /i "%2" == "" ( 
  build_8953.bat -j8
  ) else (
  build_8953.bat %~2
  )
  cd ../../../
) else if /i "%1" == "tz" (
   cd TZ.BF.4.0.5/trustzone_images/build/ms
   if /i "%2" == "" (
   build.cmd CHIPSET=msm8953 devcfg sampleapp 
   ) else (
   build.cmd CHIPSET=msm8953 devcfg sampleapp %~2
   )
   cd ../../../../
) else if /i "%1" == "adsp" (
  cd ADSP.8953.2.8.2/adsp_proc
  if /i "%2" == "" (
  python build/build.py -c msm8953 -o all
  ) else (
  python build/build.py -c msm8953 -o clean
  )
  cd ../../
) else if /i "%1" == "" (
  cd MSM8953.LA.2.0/common/build
  python build.py
  cd ../../../
) else (
  echo invalid param!
)