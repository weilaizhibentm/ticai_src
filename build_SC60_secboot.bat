@echo off

call env.bat
if /i "%1" == "newkey" (
  cd MSM8953.LA.2.0
  cd common\sectools\resources\data_prov_assets\Signing\Local\oem_certs\
  call makeCrt.bat
  cd ..\..\..\..\..\..\..\
  cd ..
) else if /i "%1" == "sec.dat" (
  cd MSM8953.LA.2.0
  python common/sectools/sectools.py fuseblower -p 8953 -g -v
  cp common\sectools\fuseblower_output\v2\sec.dat common\sectools\resources\build\fileversion2\sec.dat
  cd ..
) else if /i "%1" == "" (
  cd MSM8953.LA.2.0
  python common/sectools/sectools.py secimage -m . -p 8953 -sa
  python common/sectools/copy_sec_img.py
  python common/sectools/sectools.py  secimage  -i common/sectools/prog_emmc_firehose_8953_ddr_quectel.mbn  -c common/sectools/config/8953/8953_secimage.xml -g prog_emmc_firehose_ddr -sa
  cd common\build
  python build.py
  cd ../../../
) else (
  echo invalid param!
)
