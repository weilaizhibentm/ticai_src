/*q3a_is_chromatix_wrapper .h
 *
 * Copyright (c) 2016 Qualcomm Technologies, Inc.
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

#ifndef __q3a_is_chromatix_wrapper_H__
#define __q3a_is_chromatix_wrapper_H__

#include "is_interface.h"


/**************************************/
/* Functions Declaration */

void parse_EIS_chromatix(is_chromatix_info_t *chromatix_info, void *chromatix3APtr);

#endif /*__q3a_is_chromatix_wrapper_H__*/


