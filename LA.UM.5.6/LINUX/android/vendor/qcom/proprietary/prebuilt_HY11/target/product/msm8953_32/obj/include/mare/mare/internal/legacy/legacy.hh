// --~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~--
// Copyright 2013-2015 Qualcomm Technologies, Inc.
// All rights reserved.
// Confidential and Proprietary – Qualcomm Technologies, Inc.
// --~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~----~--~--~--~--
#pragma once

#include <mare/internal/legacy/device.hh>
#include <mare/internal/legacy/gpukernel.hh>
#include <mare/internal/legacy/gputask.hh>
#include <mare/internal/legacy/group.hh>
#include <mare/internal/legacy/task.hh>
