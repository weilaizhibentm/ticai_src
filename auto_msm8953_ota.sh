#!/bin/bash

#basepath=$(cd `dirname $0`; pwd)
prodir=$(pwd)
#adb secure userdebug 8 msm8953
cd LA.UM.5.6/LINUX/android/

source build/envsetup.sh
source build/core/build_id.mk
lunch msm8953_64-$2

mkdir -p $OUTPUT_DIR
mkdir -p $OUTPUT_DIR/OTA
#PRIMARY_OTA_FILE="/Android/2018_3/EC0100_001/debugVersion/1.0.0T/EC0100_001/180425/Zip/CPST1-01-01.1.0.0.zip"
if [ -n "$PRIMARY_OTA_FILE" ]; then 
IFS=","
# 打印由"/"分割的文件夹名字
OTA_CONFIG=$OUTPUT_DIR/OTA/ota.ini
echo '[' > $OTA_CONFIG
for name in $PRIMARY_OTA_FILE
do
    if [  -f "$name" ]; then
    baseFile=target_`basename $name`  
    baseFileNoexe="${baseFile%.*}"
    baseversion="${baseFileNoexe#*target_$VENDOR_PRODUCT_MODEL.}"
    dirPath=`dirname $name`
    formTargetFile=$dirPath/$baseFile
    echo "ota formVersion->$baseversion"
    echo "ota toVersion->$TARGET_VERSION"
    OtaFile=$OUTPUT_DIR/OTA/$VENDOR_PRODUCT_MODEL-$TARGET_VERSION-for-$baseversion-update.zip
    echo "make ota : $formTargetFile to $1 for $OtaFile----"
    ./build/tools/releasetools/ota_from_target_files --block -i $formTargetFile $1 $OtaFile
    echo "    {
						"\"Name"\": "\"$VENDOR_PRODUCT_MODEL-$TARGET_VERSION-for-$baseversion-update.zip"\",
						"\"TargetVersion"\": "\"$TARGET_VERSION"\",
						"\"BaseVersion"\": "\"$baseversion"\"
						}," >> $OTA_CONFIG
    else
     continue 
    fi
done
echo ']' >> $OTA_CONFIG
IFS="$OLD_IFS"
fi
cd ${prodir}

